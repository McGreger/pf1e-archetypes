_id: S2iIR7YMtfIkuCPw
_key: '!items!S2iIR7YMtfIkuCPw'
img: icons/sundries/books/book-symbol-triangle-silver-purple.webp
name: Ritualist
system:
  classSkills:
    apr: true
    crf: true
    kar: true
    kdu: true
    ken: true
    kge: true
    khi: true
    klo: true
    kna: true
    kno: true
    kpl: true
    kre: true
    lin: true
    spl: true
    umd: true
  description:
    value: >-
      <p><strong>Source</strong>: Chronicle of Legends pg. 12<br
      />Long-forgotten occult rituals offer great power at a terrible price.
      While most traditional spellcasters consider these occult rituals
      dangerous and unreliable, some spellcasters hone their skills with such
      rituals, making the strange spells more likely to succeed and less likely
      to cause unintended harm. As ritualists deepen their understanding of
      ritual magic, they broaden their understanding of all things occult,
      becoming true masters of mysteries which others have no ability to
      comprehend. Even those uninitiated in the art of magic can cast rituals
      and interact with the fabric of magic, but their rituals pale in
      comparison to the power of those who have sacrificed, specialized,
      studied, and trained to perfect such arts.</p><p>Becoming a ritualist
      requires a vast amount of knowledge, time, and training. Those who take up
      the calling of ritualist are typically high-ranking members of esoteric
      orders, though their pursuits often lead them down paths that even their
      peers versed in the occult shy away from. By seeking and honing such
      dangerous magic, ritualists regularly risk death&mdash;or worse. If a
      ritualist reaches the height of her power before losing sight of her soul
      or becoming lost in the dark voids between worlds, she becomes a force
      beyond reckoning, able to learn and perform powerful rituals at alarming
      speeds.</p><p>Ritualists who repeatedly put their peers in grave danger
      rarely last long as members of organized groups, often becoming outcasts.
      These outcast ritualists are typically some combination of fatalistic,
      obsessive, and unpredictable in their search for occult
      power.</p><h2>Requirements</h2><p>To qualify to become a ritualist, a
      character must fulfill all of the following criteria.
      </p><p><strong>Skills</strong>: Knowledge (arcana or history) 8 ranks. <br
      /><strong>Spells</strong>: Ability to cast 3rd-level spells. <br
      /><strong>Special</strong>: The character must be a member of a group that
      regularly deals with the occult, such as the Esoteric Order of the
      Palatine Eye or the Night Heralds. The character must have successfully
      cast at least one occult ritual as the primary caster.</p><h2>Class
      Skills</h2><p>The Ritualist's class skills are Appraise (Int), Knowledge
      (all) (Int), Linguistics (Int), Spellcraft (Int), and Use Magic Device
      (Cha)..</p><p><strong>Skill Points at each Level</strong>: 2 + Int
      modifier.<br /><strong>Hit Die</strong>: d6.</p><h2>Class
      Features</h2><table class="inner" style="width: 96.9314%;"><tbody><tr><td
      style="width: 6.92057%;"><strong>Level</strong></td><td style="width:
      13.3774%;"><strong>Base Attack Bonus</strong></td><td style="width:
      7.26257%;"><strong>Fort Save</strong></td><td style="width:
      6.51769%;"><strong>Ref Save</strong></td><td style="width:
      7.26257%;"><strong>Will Save</strong></td><td style="width:
      29.9814%;"><strong>Special</strong></td><td style="width:
      28.3054%;"><strong>Spells Per Day</strong></td></tr><tr><td style="width:
      6.92057%;">1st</td><td style="width: 13.3774%;">+0</td><td style="width:
      7.26257%;">+0</td><td style="width: 6.51769%;">+0</td><td style="width:
      7.26257%;">+1</td><td style="width: 29.9814%;">Esoteric discovery, student
      of mysteries</td><td style="width: 28.3054%;">+1 level of existing
      spellcasting class</td></tr><tr><td style="width: 6.92057%;">2nd</td><td
      style="width: 13.3774%;">+1</td><td style="width: 7.26257%;">+1</td><td
      style="width: 6.51769%;">+1</td><td style="width: 7.26257%;">+1</td><td
      style="width: 29.9814%;">Stabilize ritual</td><td style="width:
      28.3054%;">+1 level of existing spellcasting class</td></tr><tr><td
      style="width: 6.92057%;">3rd</td><td style="width: 13.3774%;">+1</td><td
      style="width: 7.26257%;">+1</td><td style="width: 6.51769%;">+1</td><td
      style="width: 7.26257%;">+2</td><td style="width: 29.9814%;">Esoteric
      discovery, occult expertise</td><td style="width: 28.3054%;">+1 level of
      existing spellcasting class</td></tr><tr><td style="width:
      6.92057%;">4th</td><td style="width: 13.3774%;">+2</td><td style="width:
      7.26257%;">+1</td><td style="width: 6.51769%;">+1</td><td style="width:
      7.26257%;">+2</td><td style="width: 29.9814%;">Cautious rituals</td><td
      style="width: 28.3054%;">+1 level of existing spellcasting
      class</td></tr><tr><td style="width: 6.92057%;">5th</td><td style="width:
      13.3774%;">+2</td><td style="width: 7.26257%;">+2</td><td style="width:
      6.51769%;">+2</td><td style="width: 7.26257%;">+3</td><td style="width:
      29.9814%;">Expediated rituals</td><td style="width: 28.3054%;">+1 level of
      existing spellcasting class</td></tr><tr><td style="width:
      6.92057%;">6th</td><td style="width: 13.3774%;">+3</td><td style="width:
      7.26257%;">+2</td><td style="width: 6.51769%;">+2</td><td style="width:
      7.26257%;">+3</td><td style="width: 29.9814%;">Esoteric discovery</td><td
      style="width: 28.3054%;">+1 level of existing spellcasting
      class</td></tr><tr><td style="width: 6.92057%;">7th</td><td style="width:
      13.3774%;">+3</td><td style="width: 7.26257%;">+2</td><td style="width:
      6.51769%;">+2</td><td style="width: 7.26257%;">+4</td><td style="width:
      29.9814%;">Independent rituals</td><td style="width: 28.3054%;">+1 level
      of existing spellcasting class</td></tr><tr><td style="width:
      6.92057%;">8th</td><td style="width: 13.3774%;">+4</td><td style="width:
      7.26257%;">+3</td><td style="width: 6.51769%;">+3</td><td style="width:
      7.26257%;">+4</td><td style="width: 29.9814%;">Focused rituals</td><td
      style="width: 28.3054%;">+1 level of existing spellcasting
      class</td></tr><tr><td style="width: 6.92057%;">9th</td><td style="width:
      13.3774%;">+4</td><td style="width: 7.26257%;">+3</td><td style="width:
      6.51769%;">+3</td><td style="width: 7.26257%;">+5</td><td style="width:
      29.9814%;">Esoteric discovery</td><td style="width: 28.3054%;">+1 level of
      existing spellcasting class</td></tr><tr><td style="width:
      6.92057%;">10th</td><td style="width: 13.3774%;">+5</td><td style="width:
      7.26257%;">+3</td><td style="width: 6.51769%;">+3</td><td style="width:
      7.26257%;">+5</td><td style="width: 29.9814%;">Master of mysteries</td><td
      style="width: 28.3054%;">+1 level of existing spellcasting
      class</td></tr></tbody></table><p><br />The following are the class
      features of the ritualist prestige class. </p><p><strong>Spells per
      Day</strong>: When a ritualist gains a level, she gains new spells per day
      as if she had also gained a level in a spellcasting class she belonged to
      before adding the prestige class. She does not, however, gain any other
      benefits a character of that class would have gained, except for
      additional spells per day, spells known (if a spontaneous spellcaster),
      and an increased effective level of spellcasting. If a character has more
      than one spellcasting class, she must decide to which class she adds the
      new level for the purpose of determining spells per day.
      </p><p><strong>Esoteric Discovery (Ex)</strong>: At 1st level, 3rd level,
      and every 3 levels thereafter, a ritualist selects one skill. When using
      that skill to cast an occult ritual, the ritualist can take 10 on the
      skill check, but only once per skill during any given ritual.
      </p><p><strong>Student of Mysteries (Ex)</strong>: A ritualist adds half
      her ritualist level (minimum 1) as a bonus on Intelligence checks made to
      learn new occult rituals. </p><p><strong>Stabilize Ritual (Su)</strong>:
      At 2nd level, once per day as a full-round action, a ritualist can refocus
      a ritual that has been paused, removing any increases to the
      ritual&rsquo;s skill check DCs that have accrued thus far due to pausing
      the ritual. </p><p><strong>Occult Expertise (Ex)</strong>: At 3rd level, a
      ritualist adds half her class level as a bonus on all skill checks when
      using occult skill unlocks. Additionally, a ritualist can take 10 on skill
      checks when using occult skill unlocks with skills chosen with the
      esoteric discovery ability. </p><p><strong>Cautious Rituals (Su)</strong>:
      At 4th level, once per day when beginning to cast a ritual as the primary
      caster, a ritualist can decide to cast the ritual as a cautious ritual.
      The DCs of a cautious ritual&rsquo;s skill checks increase by half the
      ritual&rsquo;s level (rounded down, minimum 1). However, if the ritual
      fails, there is a cumulative 10% chance per ritualist level (40% at 4th
      level, 50% at 5th level, and so on) that the ritual causes none of its
      usual failure or backlash effects. The ritual still causes backlash if it
      succeeds.</p><p>At 6th level, the ritualist&rsquo;s successful
      cautious rituals do not cause any backlash to any of their secondary
      casters.</p><p>At 8th level, a ritualist can cast a cautious ritual
      without increasing the ritual&rsquo;s skill check DCs.
      </p><p><strong>Expedited Rituals (Su)</strong>: At 5th level, once per day
      when beginning to cast a ritual as the primary caster, a ritualist can
      decide to cast the ritual as an expedited ritual. The DCs of an expedited
      ritual&rsquo;s skill checks increase by half the ritual&rsquo;s level
      (rounded down). However, the ritualist can cast the ritual as soon as its
      casters succeed at the requisite number of skill checks rather than
      waiting for the ritual&rsquo;s full casting time to elapse. A ritual can
      be both cautious and expedited, but the penalties stack.</p><p>At 9th
      level, a ritualist can cast an expedited ritual without increasing the
      ritual&rsquo;s skill check DCs. </p><p><strong>Independent Rituals
      (Su)</strong>: At 7th-level, a ritualist becomes so adept at casting
      rituals that secondary casters are unnecessary. Once per day when
      beginning to cast a ritual as the primary caster, the ritualist can decide
      to cast the ritual as an independent ritual. The ritualist gains a bonus
      on all skill checks attempted as part of casting the ritual equal to half
      the ritualist&rsquo;s level, and she can always take 10 when using skills
      she chose for her esoteric discovery ability. However, none of the
      ritual&rsquo;s secondary casters, if any, can attempt any of the
      ritual&rsquo;s skill checks. The ritualist can still include secondary
      casters as part of a ritual, if it allows secondary casters, to grant them
      the ritual&rsquo;s effects.</p><p>An independent ritual can also be cast
      as an expedited or cautious ritual (or both). </p><p><strong>Focused
      Rituals (Su)</strong>: At 8th level, a ritualist can maintain a
      ritual&rsquo;s focus even under dire circumstances. While performing a
      ritual as the primary caster, the ritualist can continue the ritual even
      if some or all of its secondary casters are incapacitated, killed, or
      moved out of range of the ritual. </p><p><strong>Master of Mysteries
      (Su)</strong>: At 10th level, a ritualist&rsquo;s knowledge of the occult
      reaches unfathomable depths, and she can perform rituals with disturbing
      alacrity. Once per day when casting a ritual as the primary caster, the
      ritualist can reduce the casting time from 10 minutes per ritual level to
      1 round per ritual level, or from 1 hour per ritual level to 1 minute per
      ritual level. One of the ritual&rsquo;s casters attempts a check every
      round or every minute of the casting instead of every 10 minutes or every
      hour, as appropriate</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - _index: 0
        level: 1
        name: Esoteric Discovery (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.1xTrhaubiNTk4SJJ
      - _index: 1
        level: 1
        name: Student of Mysteries (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Y1LZmoF3BpLMgUey
      - _index: 2
        level: 2
        name: Stabilize Ritual (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.aIOPV6DyYP3h7pi7
      - _index: 3
        level: 3
        name: Occult Expertise (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.fo5nSfrQZ82biFe9
      - _index: 4
        level: 4
        name: Cautious Rituals (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.tIoU1UHn6WV4pmg9
      - _index: 5
        level: 5
        name: Expedited Rituals (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.cSnA0wsOC3PU1m8n
      - _index: 6
        level: 7
        name: Independent Rituals (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.pHJV0PqXTuZpwcVB
      - _index: 7
        level: 8
        name: Focused Rituals (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.gunGZWKgXSRusGyP
      - _index: 8
        level: 10
        name: Master of Mysteries (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.7zxYOiud8r6FRCQT
  savingThrows:
    will:
      value: high
  skillsPerLevel: 2
  subType: prestige
  tag: ritualist
type: class

