_id: 5eb7Xw8FCUPjU54w
_key: '!items!5eb7Xw8FCUPjU54w'
img: icons/equipment/shield/pavise-wooden-wings-blue-white.webp
name: Justicar
system:
  bab: high
  classSkills:
    apr: true
    blf: true
    crf: true
    dip: true
    int: true
    pro: true
    sur: true
  description:
    value: >-
      <p><strong>Source</strong>: Pathfinder #8: Seven Days to the Grave pg.
      69<br />Justiciars embody law and civilization wherever they go, from the
      most corrupt depths of a city slum to the wildest frontier lands. They
      arbitrate disputes, deal with criminals, and establish law where there is
      none. In pleasant times they are diplomats, in dangerous ones they are
      judge, jury, and executioner. Their feet leave trails destined to become
      great roads, and their decrees carry the force of law. Being a justiciar
      is a serious duty and is not taken lightly&mdash;they tend to wear down
      over time, weathered not from travel and sunlight but from the heavy
      burden of carrying civilization forward into the future. A rare few pursue
      heretics of their own religion, keeping the faith stable and weeding out
      unruly elements. Each is sworn to uphold a religious or secular
      code.</p><p>Justiciars are usually clerics or paladins of Abadar, but
      order-minded individuals of other classes (particularly fighters, monks,
      and wizards) sometimes heed the calling to tend the roots of enlightened
      society.</p><h2>Requirements</h2><p>To qualify to become a justiciar, a
      character must fulfill all of the following
      criteria:</p><p><strong>Alignment</strong>: Any lawful.<br
      /><strong>Skills</strong>: Diplomacy 4 ranks, Knowledge (local) 8 ranks,
      and 6 ranks total in Craft, Knowledge (arcana, architecture and
      engineering, history, nobility and royalty, or religion), or Profession
      skills in any combination.<br /><strong>Feats</strong>: Investigator or
      Negotiator, prof iciency with any crossbow.<br /><strong>Special</strong>:
      A justiciar must be appointed by a lawful religious or secular authority,
      typically a governing official of higher rank than the
      character.</p><h2>Class Skills</h2><p>The Justicar's class skills are
      Appraise, Bluff, Craft, Diplomacy, Gather Information, Intimidate,
      Knowledge (any), Profession, Sense Motive, Speak Language, Survival.<br
      /><br /><strong>Skill Points at each Level</strong>: 4 + Int modifier.<br
      /><strong>Hit Die</strong>: d8.</p><h2>Class Features</h2><table
      class="inner" style="width: 96.9314%;"><tbody><tr><td style="width:
      8.45294%;"><strong>Level</strong></td><td style="width:
      19.8524%;"><strong>Base Attack Bonus</strong></td><td style="width:
      11.3594%;"><strong>Fort Save</strong></td><td style="width:
      10.0559%;"><strong>Ref Save</strong></td><td style="width:
      11.1732%;"><strong>Will Save</strong></td><td style="width:
      38.7337%;"><strong>Special</strong></td></tr><tr><td style="width:
      8.45294%;">1st</td><td style="width: 19.8524%;">+1</td><td style="width:
      11.3594%;">+2</td><td style="width: 10.0559%;">+2</td><td style="width:
      11.1732%;">+0</td><td style="width: 38.7337%;">Aura of law, authority,
      lawkeeper</td></tr><tr><td style="width: 8.45294%;">2nd</td><td
      style="width: 19.8524%;">+2</td><td style="width: 11.3594%;">+3</td><td
      style="width: 10.0559%;">+3</td><td style="width: 11.1732%;">+0</td><td
      style="width: 38.7337%;">Sure shot</td></tr><tr><td style="width:
      8.45294%;">3rd</td><td style="width: 19.8524%;">+3</td><td style="width:
      11.3594%;">+3</td><td style="width: 10.0559%;">+3</td><td style="width:
      11.1732%;">+1</td><td style="width:
      38.7337%;">Oathmark</td></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr></tbody></table><p><br
      />The following are class features of the justiciar.</p><p><strong>Weapon
      and Armor Proficiency</strong>: Justiciars gain no additional weapon or
      armor proficiencies.</p><p><strong>Aura of Law (Ex)</strong>: The power of
      a justiciar&rsquo;s aura of law (see the <em>detect law</em> spell) is
      equal to her character level. If the justiciar has cleric or paladin
      levels, the power of her aura of law is equal to her character level
      +4.</p><p><strong>Authority (Su)</strong>: A justiciar serves a religious
      or legal code and has absolute authority (granted by a higher-ranking
      agent of that code) to enforce it. This means that if the justiciar
      catches criminals, she may judge them guilty (given sufficient proof ) and
      order their execution, or do it herself if need be. Because the justiciar
      must act within the law, there is rarely any friction between her and
      established authorities other than disputes about jurisdiction and
      challenges from other forms of authority. (A religiously endorsed
      justiciar, for example, might come into conflict with secular governors
      who take exception to the execution of their subjects.) A justiciar may
      deputize others to aid her in her tasks, although she is responsible for
      their actions in her name.</p><p>As part of a Diplomacy and Intimidate
      check regarding matters of the law and her authority, a justiciar may
      exert her authority to grant herself a +4 bonus on these checks. This
      ability negates any need to carry a badge or writ of office, and is mainly
      used to convince those unfamiliar with the justiciar of her legal powers.
      For example, a justiciar can use this ability to check an angry mob intent
      on lynching a jailed prisoner, or convince a stubborn mayor that
      interfering with the justiciar&rsquo;s efforts is likely to anger the lord
      they both work for. Even against individuals who don&rsquo;t acknowledge
      his code of laws (such as thieves or warriors from other lands), the
      justicar&rsquo;s fervor still grants these skill bonuses. She does not,
      however, gain these bonuses in situations where her code of laws would not
      apply or hold no bearing (like against monsters or savages). The GM
      ultimately adjudicates who the justiciar&rsquo;s authority applies
      against.</p><p>In addition, a justiciar&rsquo;s knowledge of her code is
      magically flawless, as if she were mentally reviewing a perfect copy of
      the code. If the source of the code changes (such as a decree from the
      church&rsquo;s high priest or a new law created by a king), she instantly
      knows it. This perfect knowledge means she immediately recognizes any
      misquoting of the law (deliberate or accidental), and many justiciars
      consider it their duty to review the law book in remote settlements to
      make sure there are no errors in transcription or translation. A justiciar
      may always take 10 on Knowledge checks regarding the code, even when
      rushed or threatened.</p><p><strong>Lawkeeper</strong>: Justiciars are not
      allowed to violate their code or any oath or contract they willingly agree
      to, nor can they go against the spirit of it while holding to the letter.
      A justiciar who willingly does so loses all prestige class abilities until
      she receives an <em>atonement</em> spell from her religious superiors or
      an official pardon from her secular superiors.</p><p><strong>Sure
      Shot</strong>: A justiciar is skilled with crossbows. At 2nd level, she
      gains the ability to make a sure shot. A number of times per day equal to
      her class level, a justiciar may add her Charisma bonus on an attack roll
      made with a crossbow and deal 1 extra point of damage per justiciar
      level.</p><p><strong>Oathmark (Su)</strong>: This ability magically seals
      an oath or agreement between two people. The justiciar chooses two willing
      creatures and presses her authority upon them. By accepting her authority
      and stating their agreement on something (whether a verbal promise,
      written contract, treaty, or the like), the two creatures are magically
      linked so that if one breaks the agreement, the other knows it. The
      expiration of the link does not explain the exact nature of the betrayal,
      only that it has occurred. If one target dies, the link ends and the other
      target knows the other party is dead.</p><p>Direct and indirect attempts
      to violate the agreement end the link; swearing not to kill someone and
      then hiring someone else to do it is a violation, as is hiring assassins
      before the agreement with the orders to kill the person later. The link
      cannot penetrate other planes, areas where magic does not function, or
      spells such as mind blank that block mind reading. Once the interference
      ends, the link resumes and determines if the agreement has been broken.
      The other person in the link knows if the link is blocked, but not why.<br
      /><br />The creature who breaks the agreement is cursed with a raised
      physical mark on the forehead (or other obvious place) indicating his
      willing violation of the agreement. The target takes a &ndash;4 penalty on
      Diplomacy and Gather Information checks when dealing with those who
      dislike oathbreakers. Magical attempts to remove the mark&mdash;such as
      via <em>remove curse</em>&mdash;require a successful caster level check
      (DC 10 + justiciar&rsquo;s character level). Even if the curse is removed,
      the other party in the agreement still knows of the betrayal. The mark is
      colored and textured as the justicar chooses, and cannot be hidden with
      makeup, tattoos, or scars, although greater physical obstructions (such as
      a long wig or low-hanging hat) can conceal it.</p><p>This is a permanent
      divination effect. Creating a link is a standard action. The justiciar may
      use this ability up to three times per day.</p>
  links:
    classAssociations:
      - _index: 0
        level: 1
        name: Aura of Law (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.PgeWNSXKjNi4AcNy
      - _index: 1
        level: 1
        name: Authority (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.YMWTuWtjAsmoBgmP
      - _index: 2
        level: 1
        name: Lawkeeper
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.sJGZNRlCZPABWoL4
      - _index: 3
        level: 3
        name: Oathmark (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Yhggkol6uVcgsGPs
      - _index: 4
        level: 2
        name: Sure Shot
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.k5S5PArYaCZhUqnh
  savingThrows:
    fort:
      value: high
    ref:
      value: high
  skillsPerLevel: 4
  subType: prestige
  tag: justicar
type: class

