_id: IGXXBalNXNyPUNko
_key: '!items!IGXXBalNXNyPUNko'
img: icons/equipment/head/hood-cloth-teal.webp
name: Twilight Talon
system:
  bab: med
  classSkills:
    acr: true
    blf: true
    dev: true
    dip: true
    dis: true
    esc: true
    int: true
    kge: true
    klo: true
    kno: true
    kre: true
    lin: true
    per: true
    ste: true
    sur: true
  description:
    value: >-
      <p>Of all the members of the Eagle Knights, Twilight Talons often pay the
      highest price for the freedom the Eagle Knights work so hard to achieve,
      sacrificing years of their lives to serve as spies in nations whose
      cultures make their stomachs turn, and never receiving credit for or
      acknowledgment of their accomplishments—or even their existence. Perhaps
      even more exhausting are the moral and ethical choices a Twilight Talon
      has to make on so many of her missions. Often, such choices end up being
      difficult decisions made between two evils, and in many cases, a Twilight
      Talon ends up having to deal with the consequences of such decisions for
      months or even years to come. Yet to make no decision at all in such a
      time is perhaps the greatest evil of all, for to the Twilight Talon,
      acting in the best of intentions is always a better option than remaining
      quiet and allowing evil and cruelty to continue unopposed. Being selected
      to serve as a Twilight Talon is an honor bestowed on only the select few
      who Marshal Helena Trellis, head of the Talons, believes trustworthy
      enough to carry out the commitment and of strong enough character to be
      able to make morally difficult decisions for the best of Andoran—all
      without public recognition. To a Twilight Talon, a job well done is one
      that no one outside of the Talons ever knew occurred in the first
      place.</p><p>A candidate for the Twilight Talons rarely meets her
      commanding officer in person—it’s only after she’s been accepted into the
      fold that she begins to learn details about others in the group, and even
      then, only on a need-to-know basis. Twilight Talons train carefully in the
      arts of intrigue and spycraft, but they also endeavor to keep their combat
      skills up to snuff so that when the perfect opportunity arises, they are
      prepared to take swift and brutal advantage. When on a mission for the
      group, a Twilight Talon might tell friends and family that she’s leaving
      under some false pretense, but more often she just disappears for months
      or even years at a time. When a Twilight Talon works with a group of
      like-minded allies (as is the case when one works with an adventuring
      group), she may or may not reveal her true allegiance to her companions,
      but if she does, she must be prepared for the repercussions if those she
      trusts as allies do not keep her secret.</p><h2>Requirements</h2><p>To
      qualify to become a Twilight Talon, a character must fulfill the following
      criteria.</p><p><strong> Alignment</strong>: Any nonevil. <br
      /><strong>Feats</strong>: Catch Off-Guard, Twilight Tattoo†. <br
      /><strong>Skills</strong>: Bluff 5 ranks, Disguise 5 ranks, Knowledge
      (local) 2 ranks, Linguistics 2 ranks, Stealth 5 ranks. <br
      /><strong>Special</strong>: A senior Eagle Knight must invite the
      character into the organization.</p><h2>Class Skills</h2><p>The Twilight
      Talon's class skills are Acrobatics (Dex), Bluff (Cha), Diplomacy (Cha),
      Disable Device (Dex), Disguise (Cha), Escape Artist (Dex), Intimidate
      (Cha), Knowledge (geography) (Int), Knowledge (local) (Int), Knowledge
      (nobility) (Int), Knowledge (religion) (Int), Linguistics (Int),
      Perception (Wis), Sense Motive (Wis), Sleight of Hand (Dex), Stealth
      (Dex), and Survival (Wis).</p><p><strong>Skill Points at each
      Level</strong>: 6 + Int modifier.<br /><strong>Hit Die</strong>:
      d8.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:7.6898%"><strong>Level</strong></td><td
      style="width:15.4014%"><strong>Base Attack Bonus</strong></td><td
      style="width:8.75233%"><strong>Fort Save</strong></td><td
      style="width:7.82123%"><strong>Ref Save</strong></td><td
      style="width:8.56611%"><strong>Will Save</strong></td><td
      style="width:51.3966%"><strong>Special</strong></td></tr><tr><td
      style="width:7.6898%">1st</td><td style="width:15.4014%">+0</td><td
      style="width:8.75233%">+0</td><td style="width:7.82123%">+0</td><td
      style="width:8.56611%">+1</td><td style="width:51.3966%">Many hats, sneak
      attack +1d6</td></tr><tr><td style="width:7.6898%">2nd</td><td
      style="width:15.4014%">+1</td><td style="width:8.75233%">+1</td><td
      style="width:7.82123%">+1</td><td style="width:8.56611%">+1</td><td
      style="width:51.3966%">Enhanced tattoo 1, resourceful
      agent</td></tr><tr><td style="width:7.6898%">3rd</td><td
      style="width:15.4014%">+2</td><td style="width:8.75233%">+1</td><td
      style="width:7.82123%">+1</td><td style="width:8.56611%">+2</td><td
      style="width:51.3966%">Eye for detail</td></tr><tr><td
      style="width:7.6898%">4th</td><td style="width:15.4014%">+3</td><td
      style="width:8.75233%">+1</td><td style="width:7.82123%">+1</td><td
      style="width:8.56611%">+2</td><td style="width:51.3966%">Enhanced tattoo
      2, sneak attack +2d6</td></tr><tr><td style="width:7.6898%">5th</td><td
      style="width:15.4014%">+3</td><td style="width:8.75233%">+2</td><td
      style="width:7.82123%">+2</td><td style="width:8.56611%">+3</td><td
      style="width:51.3966%">Dead drop (minor), resourceful agent (enhanced
      critical)</td></tr><tr><td style="width:7.6898%">6th</td><td
      style="width:15.4014%">+4</td><td style="width:8.75233%">+2</td><td
      style="width:7.82123%">+2</td><td style="width:8.56611%">+3</td><td
      style="width:51.3966%">Enhanced tattoo 3, unassuming
      presence</td></tr><tr><td style="width:7.6898%">7th</td><td
      style="width:15.4014%">+5</td><td style="width:8.75233%">+2</td><td
      style="width:7.82123%">+2</td><td style="width:8.56611%">+4</td><td
      style="width:51.3966%">Sneak attack +3d6</td></tr><tr><td
      style="width:7.6898%">8th</td><td style="width:15.4014%">+6</td><td
      style="width:8.75233%">+3</td><td style="width:7.82123%">+3</td><td
      style="width:8.56611%">+4</td><td style="width:51.3966%">Enhanced tattoo
      4, dead drop (major)</td></tr><tr><td style="width:7.6898%">9th</td><td
      style="width:15.4014%">+6</td><td style="width:8.75233%">+3</td><td
      style="width:7.82123%">+3</td><td style="width:8.56611%">+5</td><td
      style="width:51.3966%">Resourceful agent (staggering
      critical)</td></tr><tr><td style="width:7.6898%">10th</td><td
      style="width:15.4014%">+7</td><td style="width:8.75233%">+3</td><td
      style="width:7.82123%">+3</td><td style="width:8.56611%">+5</td><td
      style="width:51.3966%">Enhanced tattoo 5, sneak attack
      +4d6</td></tr></tbody></table><p><br />The following are class features of
      the Twilight Talon prestige class.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.9aMzfPLzQlBglnOV]{Many
      Hats (Ex)}<br /><strong> Many Hats (Ex)</strong>: A Twilight Talon is
      often required to take the role of a specific profession for the purpose
      of getting close to a target while remaining inconspicuous, becoming an
      everyday part of that target’s life until the time to strike arrives. As
      long as a Twilight Talon is within 60 feet of another character with ranks
      in Profession (architect, barrister, clerk, courtesan, driver, engineer,
      innkeeper, librarian, merchant, or scribe), the Twilight Talon can pick up
      cues and practices in performing that job, and can use her Sense Motive
      check in place of that particular Profession check.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.DmQLMv7pbQ7Yl06u]{Sneak
      Attack}</p><p><strong> Sneak Attack</strong>: This functions as the rogue
      ability of the same name. The extra damage dealt is 1d6 at 1st level, and
      it increases by 1d6 every 3 levels thereafter. If a Twilight Talon gets a
      sneak attack bonus from another source, the bonuses to damage
      stack.</p><hr /><p><br
      />@Compendium[pf1e-archetypes.pf-prestige-features.TVs8XtTBIoL0lcdU]{Enhanced
      Tattoo (Sp)}<br /><strong> Enhanced Tattoo (Sp)</strong>: As a Twilight
      Talon gains levels, her Twilight Tattoo feat gains enhancements, allowing
      her to use certain spell-like abilities. At 2nd level and every 2 levels
      thereafter, the Twilight Talon gains access to one of the spells listed
      for that level as a spell-like ability: 2nd level—<em>disguise self</em>
      or <em>undetectable alignment</em>; 4th level— <em>alter self</em> or
      <em>invisibility</em>; 6th level—<em>glibness</em> or <em>secret
      page</em>; 8th level—<em>modify memory</em> or <em>zone of silence</em>;
      10th level—<em>mislead</em> or <em>seeming</em>. These spell-like
      abilities function at a caster level equal to the Twilight Talon’s
      character level, and each ability can be used once per day. The save DC
      for these spell like abilities is 10 + half the Twilight Talon’s level +
      her Charisma modifier.</p><hr /><p><br
      />@Compendium[pf1e-archetypes.pf-prestige-features.hlMtSs6hjw7WDeEo]{Resourceful
      Agent (Ex)}<br /><strong> Resourceful Agent (Ex)</strong>: A Twilight
      Talon often can’t risk carrying traditional weapons, as doing so would
      seem out of place for many of the covers that she employs. Instead, she
      becomes a master at maximizing the potential in everyday objects used as
      improvised weapons. At 2nd level, a Twilight Talon increases the amount of
      damage she deals with improvised weapons by one step (for example, 1d4
      becomes 1d6). At 5th level, all improvised weapons used by a Twilight
      Talon have a base critical threat range of 19–20, and she gains Critical
      Focus as a bonus feat. At 9th level, a Twilight Talon gains Staggering
      Critical as a bonus feat.</p><hr /><p><br
      />@Compendium[pf1e-archetypes.pf-prestige-features.BBRnJFFsFaVL6eqO]{Eye
      for Detail (Ex)}<br /><strong> Eye for Detail (Ex)</strong>: Part of a
      Twilight Talon’s training is the extensive study of the most common
      bureaucratic documents in Golarion’s major nations, allowing her to
      accurately forge documents. At 3rd level, when forging a document
      associated with any government, she automatically receives a +8 bonus on
      Linguistics checks to create a forgery (as though she had seen a similar
      document before).</p><hr /><p><br
      />@Compendium[pf1e-archetypes.pf-prestige-features.NMx9z5XGkClwBrx3]{Dead
      Drop (Ex)}<br /><strong> Dead Drop (Ex)</strong>: A Twilight Talon
      prepares for many contingencies and can tap her underground network for
      supplies, provided she pays into her dead drop fund beforehand. At 5th
      level, by spending an hour spreading around coin in any small town or
      larger settlement, the Twilight Talon gains the ability to access dead
      drops of equipment in the field. A Twilight Talon can pay any amount into
      her dead drop fund when she does so, but can never have more than a total
      of 100 gp per class level in her dead drop fund.</p><p>A dead drop
      functions as an emergency cache of gear that any Twilight Talon agent can
      access while in the field. Part of the process of paying into the fund
      includes arranging dead drop locations in areas that the Twilight Talon
      suspects she may visit in the future. At any point in a wilderness region,
      but no more often than once per day, the Twilight Talon can take 1d8 hours
      to seek out the closest dead drop, at which point she can purchase any
      item from the <em>Pathfinder RPG Core Rulebook</em> from the dead drop,
      deducting the price from the amount of gold she has paid into the fund at
      that date. At the GM’s discretion, gear from other sourcebooks may also be
      available in a dead drop. In particularly remote locations, a GM may rule
      that no dead drops can exist, but the GM should inform the Twilight Talon
      of this fact when expeditions to such areas are planned. Dead drops shift
      locations often, so each time a Twilight Talon seeks one out, she must
      spend the 1d8 hours to find it.</p><p>At 8th level, it takes a Twilight
      Talon only 10 minutes to pay into her dead drop fund, it takes only 30
      minutes to locate a dead drop in the wild, and her maximum dead drop fund
      increases to 2,500 gp. (This total does not increase when she gains
      additional levels.)</p><hr /><p><br
      />@Compendium[pf1e-archetypes.pf-prestige-features.iPp5gQsb8vtC4smW]{Unassuming
      Presence (Su)}<br /><strong> Unassuming Presence (Su)</strong>: A Twilight
      Talon learns to quickly direct attention away from herself using subtle
      physical cues. At 6th level, a Twilight Talon can attempt an opposed Bluff
      check as a swift action against each enemy creature within 30 feet. If she
      succeeds against every target, the Twilight Talon can immediately attempt
      a Stealth check as though she had concealment; she gains a bonus equal to
      her Twilight Talon class level on this Stealth check. She can use this
      ability up to three times per day.</p>
  links:
    classAssociations:
      - _index: 0
        level: 1
        name: Many Hats (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.9aMzfPLzQlBglnOV
      - _index: 1
        level: 1
        name: Sneak Attack
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.DmQLMv7pbQ7Yl06u
      - _index: 2
        level: 2
        name: Enhanced Tattoo (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.TVs8XtTBIoL0lcdU
      - _index: 3
        level: 2
        name: Resourceful Agent (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.hlMtSs6hjw7WDeEo
      - _index: 4
        level: 3
        name: Eye for Detail (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.BBRnJFFsFaVL6eqO
      - _index: 5
        level: 5
        name: Dead Drop (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.NMx9z5XGkClwBrx3
      - _index: 6
        level: 6
        name: Unassuming Presence (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.iPp5gQsb8vtC4smW
  savingThrows:
    will:
      value: high
  skillsPerLevel: 6
  sources:
    - id: PZO1138
      pages: '72'
  subType: prestige
  tag: twilightTalon
type: class

