_id: o54TcI1pFTDfewMV
_key: '!items!o54TcI1pFTDfewMV'
img: systems/pf1/icons/skills/red_24.jpg
name: Daivrat
system:
  classSkills:
    apr: true
    blf: true
    crf: true
    dip: true
    kar: true
    kpl: true
    lin: true
    spl: true
    umd: true
  description:
    value: >-
      <p>The empire of Kelesh is built by genies, who create towering spires and
      opulent palaces for the padishah emperor in the hopes that a future ruler
      will set their kinsfolk free. But not all Qadiran mages see genies as
      creatures to be bested and enslaved. Instead of potential slaves, these
      <em>daivrat</em> (“brothers of genies”) treat genies as powerful allies,
      wise counselors, and comrades-in-arms. Though few are foolish enough to
      consider liberating the thralls of the emperor, each daivrat considers
      herself a soldier, servant, or cousin to the genies.</p><p>Many are the
      names of the genies, and so similar that most cannot discern them. Yet the
      daivrat knows a djinni from a janni and the difference between a
      half-janni and a suli-janni. The daivrat is distinctly Qadiran, for no
      other land in Avistan or Garund has such a tradition of treating with the
      genie races. Good and neutral-aligned genies welcome a daivrat as a
      friend, and even hateful efreet know they can rely on their daivrat
      “friends,” sometimes to the daivrat’s
      detriment.</p><p><strong>Role</strong>: Daivrat fill the same role as
      standard spellcasters, plus they are effective as diplomats among the
      genie races and those allied with
      genie-kind.</p><p><strong>Alignment</strong>: Daivrat are respected by
      genies for their willingness to treat them as equals rather than potential
      slaves. They are never evil.</p><h2>Requirements</h2><p>In order to
      qualify to become a daivrat, a character must fulfill all the following
      criteria.</p><p><strong>Skills</strong>: Knowledge (arcana) 7 ranks,
      Knowledge (planes) 7 ranks, Spellcraft 5 ranks.<br
      /><strong>Feats</strong>: Spell Focus (conjuration).<br
      /><strong>Spellcasting</strong>: Ability to cast 3rd-level spells.<br
      /><strong>Languages</strong>: Aquan, Auran, Ignan or Terran.<br
      /><strong>Special</strong>: The character must have had peaceful contact
      with a true genie (djinni, efreeti, marid, or shaitan).</p><h2>Class
      Skills</h2><p>The Daivrat's class skills are Appraise (Int), Bluff (Cha),
      Craft (Int), Diplomacy (Cha), Knowledge (arcana) (Int), Knowledge (planes)
      (Int), Linguistics (Int), Spellcraft (Int), and Use Magic Device
      (Cha).</p><p><strong>Skill Points at each Level</strong>: 2 + Int
      modifier.<br /><strong>Hit Die</strong>: d6.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:8.07441%"><strong>Level</strong></td><td
      style="width:12.4097%"><strong>Base Attack Bonus</strong></td><td
      style="width:7.44879%"><strong>Fort Save</strong></td><td
      style="width:6.70391%"><strong>Ref Save</strong></td><td
      style="width:7.26257%"><strong>Will Save</strong></td><td
      style="width:23.4637%"><strong>Special</strong></td><td
      style="width:34.2644%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:8.07441%">1st</td><td style="width:12.4097%">+0</td><td
      style="width:7.44879%">+0</td><td style="width:6.70391%">+0</td><td
      style="width:7.26257%">+1</td><td style="width:23.4637%">Elemental focus,
      genie-tongue</td><td style="width:34.2644%">—</td></tr><tr><td
      style="width:8.07441%">2nd</td><td style="width:12.4097%">+1</td><td
      style="width:7.44879%">+1</td><td style="width:6.70391%">+1</td><td
      style="width:7.26257%">+1</td><td style="width:23.4637%">Spell-fetch
      (3rd)</td><td style="width:34.2644%">+1 level of existing arcane
      spellcasting class</td></tr><tr><td style="width:8.07441%">3rd</td><td
      style="width:12.4097%">+1</td><td style="width:7.44879%">+1</td><td
      style="width:6.70391%">+1</td><td style="width:7.26257%">+2</td><td
      style="width:23.4637%">Genie's friend (+2)</td><td
      style="width:34.2644%">+1 level of existing arcane spellcasting
      class</td></tr><tr><td style="width:8.07441%">4th</td><td
      style="width:12.4097%">+2</td><td style="width:7.44879%">+1</td><td
      style="width:6.70391%">+1</td><td style="width:7.26257%">+2</td><td
      style="width:23.4637%">Elemental attunement</td><td
      style="width:34.2644%">+1 level of existing arcane spellcasting
      class</td></tr><tr><td style="width:8.07441%">5th</td><td
      style="width:12.4097%">+2</td><td style="width:7.44879%">+2</td><td
      style="width:6.70391%">+2</td><td style="width:7.26257%">+3</td><td
      style="width:23.4637%">Spell-fetch (6th)</td><td style="width:34.2644%">+1
      level of existing arcane spellcasting class</td></tr><tr><td
      style="width:8.07441%">6th</td><td style="width:12.4097%">+3</td><td
      style="width:7.44879%">+2</td><td style="width:6.70391%">+2</td><td
      style="width:7.26257%">+3</td><td style="width:23.4637%">Zhyen
      familiar</td><td style="width:34.2644%">+1 level of existing arcane
      spellcasting class</td></tr><tr><td style="width:8.07441%">7th</td><td
      style="width:12.4097%">+3</td><td style="width:7.44879%">+2</td><td
      style="width:6.70391%">+2</td><td style="width:7.26257%">+4</td><td
      style="width:23.4637%">Greater elemental focus</td><td
      style="width:34.2644%">+1 level of existing arcane spellcasting
      class</td></tr><tr><td style="width:8.07441%">8th</td><td
      style="width:12.4097%">+4</td><td style="width:7.44879%">+3</td><td
      style="width:6.70391%">+3</td><td style="width:7.26257%">+4</td><td
      style="width:23.4637%">Spell-fetch (8th)</td><td style="width:34.2644%">+1
      level of existing arcane spellcasting class</td></tr><tr><td
      style="width:8.07441%">9th</td><td style="width:12.4097%">+4</td><td
      style="width:7.44879%">+3</td><td style="width:6.70391%">+3</td><td
      style="width:7.26257%">+5</td><td style="width:23.4637%">Genie's friend
      (+4)</td><td style="width:34.2644%">+1 level of existing arcane
      spellcasting class</td></tr><tr><td style="width:8.07441%">10th</td><td
      style="width:12.4097%">+5</td><td style="width:7.44879%">+3</td><td
      style="width:6.70391%">+3</td><td style="width:7.26257%">+5</td><td
      style="width:23.4637%">Genie-kin</td><td style="width:34.2644%">+1 level
      of existing arcane spellcasting class</td></tr></tbody></table><p><br
      />All of the following are Class Features of the daivrat prestige
      class.</p><p><strong>Weapon and Armor Proficiency</strong>: A daivrat
      gains no proficiency with any weapon or
      armor.</p><p><strong>Spells</strong>: At the indicated levels, a daivrat
      gains new spells per day as if he had also gained a level in a
      spellcasting class he belonged to before adding the prestige class. He
      does not gain other benefits a character of that class would have gained,
      except for additional spells per day, spells known (if he is a spontaneous
      spellcaster), and an increased effective level of spellcasting. If a
      character had more than one spellcasting class before becoming a daivrat,
      he must decide to which class he adds the new level for purposes of
      determining spells per day.</p><p><strong>Elemental Focus (Su)</strong>:
      At 1st level, the daivrat gains a measure of connection with elemental
      powers. Choose one of the following descriptors: acid, air, cold, earth,
      electricity, fire, or water. When casting spells of the chosen descriptor,
      the daivrat is considered to possess the Spell Focus feat for that spell.
      Only one Spell Focus or Greater Spell Focus applies to the casting of a
      spell.</p><p><strong>Genie-Tongue</strong>: A daivrat is well-versed in
      communicating with genies and genie-kin. A daivrat gains a +2 bonus on
      Charisma-based checks to influence the attitudes of genies and allies of
      genies, and a +4 bonus to Knowledge checks regarding
      genies.</p><p><strong>Spell-Fetch (Su)</strong>: Once per day, while
      preparing spells (or spell slots, for spontaneous casters), a daivrat can
      conjure a minor genie to find an unusual spell for him. The daivrat
      chooses one spell he does not know, such as a spell from a prohibited
      school or from another class’s spell list, and sends the genie to fetch
      magical energy matching that spell. The genie returns after 10 minutes and
      presents the daivrat with the matching spell energy.</p><p>If the daivrat
      casts spells like a wizard, for the next 24 hours he can prepare the
      fetched spell as if it were in his spellbook. If he casts spells like a
      sorcerer, for the next 24 hours he may cast the fetched spell as if he
      knew it.</p><p>At 2nd level, the genie can retrieve any spell of 3rd level
      or lower. At 5th level, the genie can retrieve any spell of 6th level or
      lower. At 8th level, the genie can retrieve any spell of 8th level or
      lower. The genie is incorporeal and has no other powers or abilities (it
      cannot be used to scout, send messages, and so on) and vanishes once it
      delivers the spell.</p><p>Preparing or casting the fetched spell is more
      difficult than using spells the daivrat actually knows. If the fetched
      spell is on the daivrat’s spellcasting class’s spell list, it is treated
      as 1 spell level higher than normal; otherwise it is treated as 2 spell
      levels higher than normal.</p><p><strong>Genie’s Friend (Su)</strong>: At
      3rd level, a daivrat gains a +2 bonus to his caster level when conjuring
      genies. At 9th level, this bonus increases to +4.<br /><br
      /><strong>Elemental Attunement (Ex)</strong>: At 4th level, the daivrat
      gains energy resistance 5 against acid, cold, electricity, or
      fire.</p><p><strong>Zhyen Familiar</strong>: At 6th level, a daivrat can
      select a zhyen (see the Bestiary chapter) to serve as his familiar,
      replacing any familiar he already possesses, as if he had the Improved
      Familiar feat.</p><p><strong>Greater Elemental Focus (Su)</strong>: At 7th
      level, the daivrat’s connection with his chosen element deepens. When
      casting spells with the descriptor chosen for his elemental focus ability,
      the daivrat is now considered to possess the Greater Spell Focus feat.
      Only one Spell Focus or Greater Spell Focus applies to the casting of a
      spell.</p><p><strong>Genie-kin (Ex)</strong>: At 10th level, the daivrat’s
      type changes to outsider (native). He gains acid, cold, electricity, and
      fire resistance 5, resistance 10 against his chosen energy type from his
      elemental attunement ability, and damage reduction 5/—.</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - _index: 0
        level: 4
        name: Elemental Attunement (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.5EdZlkGG9tQ0HIwQ
      - _index: 1
        level: 1
        name: Elemental Focus (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.UBZ0vpeEwQBSwNRK
      - _index: 2
        level: 10
        name: Genie-kin (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.ylBDE8lzVAYOrAOh
      - _index: 3
        level: 1
        name: Genie-Tongue
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.oPOknDxP2dsdvmXg
      - _index: 4
        level: 3
        name: Genie's Friend (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.DS7QqTVnyo4MAJUJ
      - _index: 5
        level: 7
        name: Greater Elemental Focus (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.es7fUZK478KHmCuA
      - _index: 6
        level: 2
        name: Spell-Fetch (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.5b0akJ4HLPTbOVWn
      - _index: 7
        level: 6
        name: Zhyen Familiar
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.iK4uyGrbIqsGoV86
  savingThrows:
    will:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO9406
      pages: '20'
  subType: prestige
  tag: daivrat
type: class

