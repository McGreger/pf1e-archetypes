_id: 7mXOXvWZgpc0t59B
_key: '!items!7mXOXvWZgpc0t59B'
img: icons/tools/scribal/ink-quill-red.webp
name: Pathfinder Savant
system:
  classSkills:
    apr: true
    crf: true
    lin: true
    per: true
    pro: true
    spl: true
    sur: true
    umd: true
  description:
    value: >-
      <p>Pathfinder savants are specialists in the theory and practice of magic,
      illuminating mysteries of the eldritch fabric that permeates existence.
      The path of the savant brings expertise in the lore of glyphs and sigils,
      knowledge of exotic spells, and the power to unlock the full potential of
      magical devices. This skill also makes savants quite valuable to
      adventuring parties, both in their mastery over ancient traps that utilize
      old magic and in their skill at identifying and utilizing magic items
      found in the field. Of course, with their focus on the intellectual
      portions of adventuring, Pathfinder savants are often ill-equipped to
      handle the tougher, more excruciating parts of a typical dungeon delve;
      melee combat, wrenching open stuck doors, and enduring dangerous and
      unhealthy environments are not the savant’s forte. As a result, they
      rarely undertake solo missions, instead allying with wellrounded groups of
      fellow adventurers in order to interact with history and strange archaic
      wonders without dealing with the grislier portions of exploration all on
      their own. To the Pathfinder savant, this is merely a logical extension of
      their preferred role in exploration, but that doesn’t protect them from
      periodic sneers and veiled insults from more physically focused
      companions. Of course, most such associates are quick to beg forgiveness
      when, inevitably, the savant is needed to decipher the workings of a
      strange magical device or to call upon their significant magical power to
      solve problems that cannot be dealt with through brawn
      alone.</p><h2>Requirements</h2><p>To qualify to become a Pathfinder
      savant, a character must fulfill the following criteria. <br
      /><strong>Feats</strong>: Magical Aptitude, any one item crafting feat.
      <br /><strong>Skills</strong>: Knowledge (arcana) 5 ranks, Spellcraft 5
      ranks, Use Magic Device 5 ranks. <br /><strong>Special</strong>: Ability
      to cast 2nd-level spells.</p><h2>Class Skills</h2><p>The Pathfinder
      Savant's class skills are Appraise (Int), Knowledge (all) (Int),
      Linguistics (Int), Perception (Wis), Spellcraft (Int), Survival (Wis), and
      Use Magic Device (Cha).</p><p><strong>Skill Points at each
      Level</strong>: 2 + Int modifier.<br /><strong>Hit Die</strong>:
      d6.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:7.6898%"><p><strong>Level</strong></p></td><td
      style="width:15.2152%"><p><strong>Base Attack Bonus</strong></p></td><td
      style="width:8.56611%"><p><strong>Fort Save</strong></p></td><td
      style="width:7.82123%"><p><strong>Ref Save</strong></p></td><td
      style="width:8.56611%"><p><strong>Will Save</strong></p></td><td
      style="width:29.7952%"><p><strong>Special</strong></p></td><td
      style="width:21.9739%"><p><strong>Spells Per
      Day</strong></p></td></tr><tr><td style="width:7.6898%"><p>1st</p></td><td
      style="width:15.2152%"><p>+0</p></td><td
      style="width:8.56611%"><p>+0</p></td><td
      style="width:7.82123%"><p>+0</p></td><td
      style="width:8.56611%"><p>+1</p></td><td style="width:29.7952%"><p>Adept
      activation, master scholar</p></td><td
      style="width:21.9739%"><p>—</p></td></tr><tr><td
      style="width:7.6898%"><p>2nd</p></td><td
      style="width:15.2152%"><p>+1</p></td><td
      style="width:8.56611%"><p>+1</p></td><td
      style="width:7.82123%"><p>+1</p></td><td
      style="width:8.56611%"><p>+1</p></td><td
      style="width:29.7952%"><p>Esoteric magic, glyph-finding</p></td><td
      style="width:21.9739%"><p>+1 level of existing class</p></td></tr><tr><td
      style="width:7.6898%"><p>3rd</p></td><td
      style="width:15.2152%"><p>+1</p></td><td
      style="width:8.56611%"><p>+1</p></td><td
      style="width:7.82123%"><p>+1</p></td><td
      style="width:8.56611%"><p>+2</p></td><td style="width:29.7952%"><p>Scroll
      master</p></td><td style="width:21.9739%"><p>+1 level of existing
      class</p></td></tr><tr><td style="width:7.6898%"><p>4th</p></td><td
      style="width:15.2152%"><p>+2</p></td><td
      style="width:8.56611%"><p>+1</p></td><td
      style="width:7.82123%"><p>+1</p></td><td
      style="width:8.56611%"><p>+2</p></td><td style="width:29.7952%"><p>Quick
      identification</p></td><td style="width:21.9739%"><p>+1 level of existing
      class</p></td></tr><tr><td style="width:7.6898%"><p>5th</p></td><td
      style="width:15.2152%"><p>+2</p></td><td
      style="width:8.56611%"><p>+2</p></td><td
      style="width:7.82123%"><p>+2</p></td><td
      style="width:8.56611%"><p>+3</p></td><td style="width:29.7952%"><p>Sigil
      master</p></td><td style="width:21.9739%"><p>+1 level of existing
      class</p></td></tr><tr><td style="width:7.6898%"><p>6th</p></td><td
      style="width:15.2152%"><p>+3</p></td><td
      style="width:8.56611%"><p>+2</p></td><td
      style="width:7.82123%"><p>+2</p></td><td
      style="width:8.56611%"><p>+3</p></td><td style="width:29.7952%"><p>Analyze
      dweomer, silence master</p></td><td style="width:21.9739%"><p>+1 level of
      existing class</p></td></tr><tr><td
      style="width:7.6898%"><p>7th</p></td><td
      style="width:15.2152%"><p>+3</p></td><td
      style="width:8.56611%"><p>+2</p></td><td
      style="width:7.82123%"><p>+2</p></td><td
      style="width:8.56611%"><p>+4</p></td><td
      style="width:29.7952%"><p>Dispelling master</p></td><td
      style="width:21.9739%"><p>+1 level of existing class</p></td></tr><tr><td
      style="width:7.6898%"><p>8th</p></td><td
      style="width:15.2152%"><p>+4</p></td><td
      style="width:8.56611%"><p>+3</p></td><td
      style="width:7.82123%"><p>+3</p></td><td
      style="width:8.56611%"><p>+4</p></td><td style="width:29.7952%"><p>Symbol
      master</p></td><td style="width:21.9739%"><p>+1 level of existing
      class</p></td></tr><tr><td style="width:7.6898%"><p>9th</p></td><td
      style="width:15.2152%"><p>+4</p></td><td
      style="width:8.56611%"><p>+3</p></td><td
      style="width:7.82123%"><p>+3</p></td><td
      style="width:8.56611%"><p>+5</p></td><td
      style="width:29.7952%"><p>Spellcasting master</p></td><td
      style="width:21.9739%"><p>+1 level of existing class</p></td></tr><tr><td
      style="width:7.6898%"><p>10th</p></td><td
      style="width:15.2152%"><p>+5</p></td><td
      style="width:8.56611%"><p>+3</p></td><td
      style="width:7.82123%"><p>+3</p></td><td
      style="width:8.56611%"><p>+5</p></td><td style="width:29.7952%"><p>Item
      master</p></td><td style="width:21.9739%"><p>+1 level of existing
      class</p></td></tr></tbody></table><p><br />The following are class
      features of the Pathfinder savant prestige class.</p><p><strong>Adept
      Activation (Ex)</strong>: A Pathfinder savant can always take 10 on Use
      Magic Device checks, except when activating an item blindly. A Pathfinder
      savant does not automatically fail a Use Magic Device check if he rolls a
      natural 1 on the check.</p><p><strong>Master Scholar (Ex)</strong>: A
      Pathfinder savant adds half his class level (minimum 1) as a bonus on
      Knowledge (arcana), Spellcraft, and Use Magic Device checks. He can always
      take 10 on Knowledge (arcana) and Spellcraft checks, even if distracted or
      endangered.</p><p><strong>Esoteric Magic (Ex)</strong>: At each class
      level beyond 1st, the Pathfinder savant chooses a spell from any class’s
      spell list and thereafter treats that spell as if it were on the spell
      list of the base spellcasting class for which he has the most levels; if
      this base spellcasting class could not normally cast that spell, it is
      treated as 1 level higher than it is on the original class’s spell list.
      If he could cast the spell using his base spellcasting class, the spell’s
      level does not increase. The spell is cast as if its type (arcane, divine,
      or psychic) were that of his base spellcasting class, and save DCs
      function as normal for spells of that class. All other restrictions of his
      normal spellcasting class apply. This ability does not grant other
      spellcasters special allowance to prepare, cast, or use spell-trigger or
      spell-completion items of esoteric spells (such as a sorcerer using a
      <em>cure light wounds</em> scroll prepared by the Pathfinder
      savant).</p><p><strong>Glyph-Finding (Ex)</strong>: At 2nd level, a
      Pathfinder savant can use Spellcraft to find writing-based magical traps
      (including glyphs, runes, sigils, and symbols) in the same way a rogue can
      use Perception to search for traps.</p><p><strong>Scroll Master
      (Su)</strong>: At 3rd level, a Pathfinder savant can use his own caster
      level instead of the item’s caster level when using a scroll or other
      spell-completion item.</p><p><strong>Quick Identification (Sp)</strong>:
      At 4th level, a Pathfinder savant can use <em>identify</em> as a swift
      action (caster level equals his character level). He can do this once per
      day per 2 class levels.</p><p><strong> Sigil Master (Su)</strong>: A
      5th level, a Pathfinder savant receives a bonus equal to his class level
      on saving throws against writing-based magical traps, and if he succeeds
      at the save, he does not trigger the trap. Such a trap is not disabled,
      and if he leaves the trap’s area and then reenters it, the trap can
      trigger again. A Pathfinder savant also receives this bonus on saving
      throws against the effects of such traps triggered by
      others.</p><p><strong>Analyze Dweomer (Sp)</strong>: At 6th level, a
      Pathfinder savant can use <em>analyze dweomer</em> for up to 1 round per
      class level per day. He can use this ability in 1-round
      increments.</p><p><strong>Silence Master (Su)</strong>: A 6th-level
      Pathfinder savant can to activate spell-trigger, spell-completion, and
      command-word items silently, substituting a magical gesture for the
      necessary words. He cannot use this ability in circumstances where he
      could not cast a spell with somatic components. He must know how to
      activate the item normally for this ability to work. Three times per day,
      he can cast a spell of 6th level or lower as if he were using a <em>silent
      metamagic rod</em>.</p><p><strong>Dispelling Master (Su)</strong>: At 7th
      level, if the Pathfinder savant prepares and casts spells like a wizard,
      he can spontaneously convert any 3rd-level (or higher-level) prepared
      spell into <em>dispel magic</em> or any 6th-level (or higher-level)
      prepared spell into <em>greater dispel magic</em>, as a good-aligned
      cleric converts prepared spells into cure spells. If he casts spells
      spontaneously, he adds <em>dispel magic</em> and <em>greater dispel
      magic</em> to his list of spells known. Every time he successfully uses
      either of these spells to make a targeted dispel or counterspell, he heals
      a number of hit points equal to the caster level of the effect dispelled
      or counterspelled.</p><p><strong> Symbol Master (Su)</strong>: At 8th
      level, a Pathfinder savant gains power over magical symbol spells (such as
      <em>symbol of death</em>). When he casts any symbol spell, the save DC to
      resist its effects, the Perception DC to notice the symbol, and the
      Disable Device DC to remove the symbol increase by 2. Once per day as an
      immediate action, the Pathfinder savant can double the bonus granted by
      his sigil master ability when he attempts a saving throw against a symbol.
      He can activate this ability after he rolls the saving throw, but must do
      so before the results are revealed. A Pathfinder savant does not
      automatically fail a saving throw against a symbol effect on a natural
      1.</p><p><strong>Spellcasting Master (Ex)</strong>: At 9th level, a
      Pathfinder savant can focus his mind three times per day as a swift
      action. Once he’s focused in this way, any spells he casts for the
      remainder of that round do not provoke attacks of opportunity. Spells with
      a duration of concentration that he casts in this round persist for a
      number of rounds after the Pathfinder savant ceases concentrating equal to
      his Intelligence, Wisdom, or Charisma modifier (whichever is highest).<br
      /><br /><strong> Item Master (Su)</strong>: At 10th level, a Pathfinder
      savant can spend an hour focusing his energies on a single noncharged
      magic item that requires an action to activate. At the end of this hour,
      that item becomes attuned to the Pathfinder savant, and from that point
      on, that specific item can use the Pathfinder savant’s caster level to
      resolve its effects rather than the item’s caster level. A Pathfinder
      savant can change which item is attuned to him by repeating the attunement
      ritual, but he loses attunement to the previous item. A Pathfinder savant
      can have only one item attuned to him at a time.</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - _index: 0
        level: 1
        name: Adept Activation (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.oQdR1gVTmSurNHHK
      - _index: 1
        level: 1
        name: Master Scholar (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.QqknuJGO7y1FALsT
      - _index: 2
        level: 2
        name: Esoteric Magic (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.awYcmua0n6xx5lx7
      - _index: 3
        level: 2
        name: Glyph-Finding (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.1q0LFGYsYEDLiteT
      - _index: 4
        level: 3
        name: Scroll Master (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.TslKdhHgUeMZoBqg
      - _index: 5
        level: 4
        name: Quick Identification (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.BUP8hYzHOL6RJA7k
      - _index: 6
        level: 5
        name: Sigil Master (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.mznovwcbTMsYfx2u
      - _index: 7
        level: 6
        name: Analyze Dweomer (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.j1M0NVPZw2wCZgZm
      - _index: 8
        level: 6
        name: Silence Master (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.E8SQsaBKkEgaG4Ut
      - _index: 9
        level: 7
        name: Dispelling Master (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.qNT13NYO2QF2HJtO
      - _index: 10
        level: 8
        name: Symbol Master (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.yclnN7drS5BsWm1d
      - _index: 11
        level: 9
        name: Spellcasting Master (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.dW3wUKKMqyp9bqq1
      - _index: 12
        level: 10
        name: Item Master (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.zrUjDcih1TjjH9XJ
  savingThrows:
    will:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO1138
      pages: '140'
    - id: PZO9211
      pages: '60'
  subType: prestige
  tag: pathfinderSavant
type: class

