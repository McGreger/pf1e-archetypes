_id: uRJRKVQFraBdTBzG
_key: '!items!uRJRKVQFraBdTBzG'
img: icons/equipment/head/hood-cloth-teal-gold.webp
name: Master Spy
system:
  bab: med
  classSkills:
    blf: true
    dev: true
    dip: true
    dis: true
    esc: true
    kar: true
    kdu: true
    ken: true
    kge: true
    khi: true
    klo: true
    kna: true
    kno: true
    kpl: true
    kre: true
    lin: true
    per: true
    ste: true
    umd: true
  description:
    value: >-
      <p><strong>Source</strong>: Advanced Player's Guide pg. 270<br
      />Unparalleled practitioners of deception, master spies rely on deceit and
      trickery over brawn and spells. A master spy is an expert at both magical
      and nonmagical means of evading detection, and quick-witted when it comes
      to improvisation. Master spies may serve countries or private interests,
      or sell their services to the highest bidder without a thought to
      long-term loyalty. </p><p>Bards and rogues, with their wide range of
      skills, are particularly well suited for the demands of spying. Rogues can
      easily pass themselves off as members of most nonmagical professions
      (though playing an armored warrior impedes their natural abilities), while
      bluffing bards can impersonate a wide range of other spellcasters well
      enough to fool a casual observer. Versatility and adaptability are the
      watchwords for espionage, and successful master spies let others complete
      their deception for them, subtly drawing targets into their machinations.
      </p><p><strong>Role</strong>: Master spies rarely work with others unless
      they are under cover. The special skills that serve them so well in the
      field ironically make them exceptional leaders, but a master spy's natural
      inclination toward secrecy pushes her away from the limelight. Note that a
      master spy among adventurers may not intend to betray them; armed heroes
      provide excellent cover for a master spy's true mission, whether or not
      they know the spy's intent. </p><p><strong>Alignment</strong>: The
      practice of espionage demands discipline and a certain amount of moral and
      ethical flexibility; more master spies are neutral than lawful or
      chaotic.</p><h2>Requirements</h2><p>To qualify to become a master spy, a
      character must fulfill all the following criteria.
      </p><p><strong>Feats</strong>: Deceitful, Iron Will. <br
      /><strong>Skills</strong>: Bluff 7 ranks, Disguise 7 ranks, Perception 5
      ranks, Sense Motive 5 ranks.</p><h2>Class Skills</h2><p>The Master Spy's
      class skills are Bluff (Cha), Diplomacy (Cha), Disable Device (Dex),
      Disguise (Cha), Escape Artist (Dex), Knowledge (all) (Int), Linguistics
      (Int), Perception (Wis), Sense Motive (Wis), Sleight of Hand (Dex),
      Stealth (Dex), and Use Magic Device (Cha).</p><p><strong>Skill Points at
      each Level</strong>: 6 + Int modifier.<br /><strong>Hit Die</strong>:
      d8.</p><h2>Class Features</h2><table class="inner" style="width:
      96.9314%;"><tbody><tr><td style="width:
      8.84365%;"><strong>Level</strong></td><td style="width:
      14.6201%;"><strong>Base Attack Bonus</strong></td><td style="width:
      8.93855%;"><strong>Fort Save</strong></td><td style="width:
      8.00745%;"><strong>Ref Save</strong></td><td style="width:
      8.75233%;"><strong>Will Save</strong></td><td style="width:
      50.4655%;"><strong>Special</strong></td></tr><tr><td style="width:
      8.84365%;">1st</td><td style="width: 14.6201%;">+0</td><td style="width:
      8.93855%;">+0</td><td style="width: 8.00745%;">+1</td><td style="width:
      8.75233%;">+1</td><td style="width: 50.4655%;">Art of deception, master of
      disguise, sneak attack +1d6</td></tr><tr><td style="width:
      8.84365%;">2nd</td><td style="width: 14.6201%;">+1</td><td style="width:
      8.93855%;">+1</td><td style="width: 8.00745%;">+1</td><td style="width:
      8.75233%;">+1</td><td style="width: 50.4655%;">Glib lie, mask
      alignment</td></tr><tr><td style="width: 8.84365%;">3rd</td><td
      style="width: 14.6201%;">+2</td><td style="width: 8.93855%;">+1</td><td
      style="width: 8.00745%;">+2</td><td style="width: 8.75233%;">+2</td><td
      style="width: 50.4655%;">Nonmagical aura 2/day, superficial
      knowledge</td></tr><tr><td style="width: 8.84365%;">4th</td><td
      style="width: 14.6201%;">+3</td><td style="width: 8.93855%;">+1</td><td
      style="width: 8.00745%;">+2</td><td style="width: 8.75233%;">+2</td><td
      style="width: 50.4655%;">Concealed thoughts, quick change, sneak attack
      +2d6</td></tr><tr><td style="width: 8.84365%;">5th</td><td style="width:
      14.6201%;">+3</td><td style="width: 8.93855%;">+2</td><td style="width:
      8.00745%;">+3</td><td style="width: 8.75233%;">+3</td><td style="width:
      50.4655%;">Elude detection, slippery mind</td></tr><tr><td style="width:
      8.84365%;">6th</td><td style="width: 14.6201%;">+4</td><td style="width:
      8.93855%;">+2</td><td style="width: 8.00745%;">+3</td><td style="width:
      8.75233%;">+3</td><td style="width: 50.4655%;">Shift
      alignment</td></tr><tr><td style="width: 8.84365%;">7th</td><td
      style="width: 14.6201%;">+5</td><td style="width: 8.93855%;">+2</td><td
      style="width: 8.00745%;">+4</td><td style="width: 8.75233%;">+4</td><td
      style="width: 50.4655%;">Sneak attack +3d6</td></tr><tr><td style="width:
      8.84365%;">8th</td><td style="width: 14.6201%;">+6</td><td style="width:
      8.93855%;">+3</td><td style="width: 8.00745%;">+4</td><td style="width:
      8.75233%;">+4</td><td style="width: 50.4655%;">Death attack, fool
      casting</td></tr><tr><td style="width: 8.84365%;">9th</td><td
      style="width: 14.6201%;">+6</td><td style="width: 8.93855%;">+3</td><td
      style="width: 8.00745%;">+5</td><td style="width: 8.75233%;">+5</td><td
      style="width: 50.4655%;">Hidden mind</td></tr><tr><td style="width:
      8.84365%;">10th</td><td style="width: 14.6201%;">+7</td><td style="width:
      8.93855%;">+3</td><td style="width: 8.00745%;">+5</td><td style="width:
      8.75233%;">+5</td><td style="width: 50.4655%;">Assumption, sneak attack
      +4d6</td></tr></tbody></table><p><br />The following are class features of
      the master spy prestige class. </p><p><strong>Weapon and Armor
      Proficiency</strong>: A master spy gains no proficiency with any weapon or
      armor. </p><p><strong>Art of Deception (Ex)</strong>: A master spy adds
      her class level to all Bluff, Disguise, and Sense Motive checks.
      </p><p><strong>Master of Disguise (Ex)</strong>: A master spy can create a
      disguise in half the time normally required. In addition, any penalties
      from assuming a disguise of a different gender, race, age, or size are
      reduced by 1. </p><p><strong>Sneak Attack (Ex)</strong>: This ability is
      exactly like the rogue ability of the same name. The extra damage dealt
      increases by +1d6 at every third level (1st, 4th, 7th, and 10th). If a
      master spy gets a sneak attack bonus from another source, the bonuses on
      damage stack. </p><p><strong>Glib Lie (Su)</strong>: A master spy of 2nd
      level or higher can deceive truth-detecting magic. A creature using this
      sort of magic against the spy must succeed on a caster level check against
      a DC of 15 + the master spy's class level to succeed (as if she were under
      the effect of a <em>glibness </em>spell); failure means the magic doesn't
      detect the spy's lies or force her to speak only the truth. This ability
      does not give the master spy the <em>glibness </em>spell's bonus on Bluff
      checks. </p><p><strong>Mask Alignment (Su)</strong>: A master spy of 2nd
      level or higher can alter her alignment aura to deceive spells that
      discern alignment (such as <em>detect evil</em>). She may choose to detect
      as any specific alignment, or to detect as no alignment at all. This
      ability does not protect against spells or effects that cause harm based
      on alignment. Masking her alignment aura is a standard action, and lasts
      until she changes it again or ends the effect. </p><p><strong>Nonmagical
      Aura (Sp)</strong>: At 3rd level, a master spy can use <em>magic aura
      </em>twice per day, but only for the purpose of making an object appear
      nonmagical. </p><p><strong>Superficial Knowledge (Ex)</strong>: A master
      spy gives the appearance of knowing more than she actually does. Starting
      at 3rd level, she can make untrained Knowledge and Profession checks
      pertaining to her cover or assumed identity as if she were trained and
      gains a bonus equal to half her level on these checks. For example, a
      master spy masquerading as a noblewoman can make untrained Knowledge
      (history) checks about the kingdom and Knowledge (nobility) checks about
      its noble and royal families as if she were trained, but she cannot make
      untrained Knowledge (nature) skill checks to identify herbs.
      </p><p><strong>Concealed Thoughts (Su)</strong>: A 4th-level master spy
      can conceal her schemes from mind-reading magic. When a creature is using
      <em>detect thoughts </em>or similar magic to read her mind, she decides
      what surface thoughts her opponent detects, and her true surface thoughts
      remain private. This ability does not protect against mental attacks or
      mind-reading that delves deeper than surface thoughts.
      </p><p><strong>Quick Change (Ex)</strong>: Starting at 4th level, a master
      spy can assume a disguise in only 2d4 rounds by taking a &ndash;10 penalty
      on her Disguise check. This penalty drops to &ndash;5 at 8th level.
      </p><p><strong>Elude Detection (Sp)</strong>: At 5th level, a master spy
      can befuddle divinations used against her as if she were under the effect
      of a <em>nondetection </em>spell with a caster level equal to her
      character level. She can suppress or resume this protection as a standard
      action. If dispelled, the spy cannot resume the <em>nondetection </em>for
      1d4 rounds. </p><p><strong>Slippery Mind (Su)</strong>: At 5th level, a
      master spy can slip away from mental control. This functions as the rogue
      advanced talent of the same name. If the spy has the slippery mind ability
      from another class, these abilities stack, but she can still only use
      slippery mind once per round. </p><p><strong>Shift Alignment
      (Su)</strong>: Starting at 6th level, a master spy's control over her aura
      improves. When she assumes a false alignment, she can choose to have all
      spells and magic items affect her as though she were that alignment; this
      includes helpful and harmful effects. For example, a neutral good master
      spy can shift her aura to lawful evil so she can pass through a doorway
      that shocks creatures that aren't lawful evil; if hit by <em>holy smite
      </em>with this shifted aura, she takes damage as if she were evil. A
      master spy can change her alignment aura from a masked alignment (as per
      her 2nd-level class feature, where effects still function based on her
      actual alignment) to a shifted alignment (as per this ability, where
      effects function based on her assumed alignment) as a standard action.
      Shifting her alignment aura is a standard action, and lasts until she
      changes it again or ends the effect. </p><p><strong>Death Attack
      (Ex)</strong>: At 8th level, a master spy learns the art of killing or
      paralyzing a foe with a careful strike. This ability functions as the
      assassin's death attack ability. If the master spy has levels in another
      class that grants the death attack ability, these levels stack with her
      master spy level to determine the DC of her death attack, even if she has
      not yet reached 8th level as a master spy. </p><p><strong>Fool Casting
      (Su)</strong>: A master spy of 8th level or higher can trick an opponent
      into believing that she has been charmed or dominated. When the master spy
      succeeds at a saving throw against a magical effect that provides ongoing
      control (such as <em>charm person</em>, <em>dominate person</em>, or a
      vampire's dominate ability), she can allow the spell to take partial
      effect. To the caster, it appears that the spy failed her saving throw,
      but the spy is not under the caster's control. If the spell provides a
      telepathic link, it functions normally, but the spy is under no obligation
      to follow the caster's commands. The master spy can dismiss a fooled spell
      as a standard action. Fooled casting can be used when the spy succeeds at
      a subsequent saving throw against an ongoing effect, such as that granted
      by slippery mind. </p><p><strong>Hidden Mind (Sp)</strong>: At 9th level,
      a master spy gains the benefit of a constant <em>mind blank </em>spell at
      a caster level equal to her character level. The spy can suppress or
      resume this protection as a standard action. If dispelled, the spy cannot
      resume the <em>mind blank </em>for 1d4 rounds. </p><p><strong>Assumption
      (Su)</strong>: The ultimate ability of the master spy is to take over
      another persona entirely, making it her own. As a full-round action, the
      spy can touch a helpless creature and shift her aura to that of her
      target. This confuses divination effects and spells, even ones as powerful
      as <em>discern location</em>, such that they register the spy as being the
      creature she has touched. This ability is not proof against the actions of
      deities or similarly powerful beings. The assumption of an identity lasts
      until the master spy ends it (a standard action) or she uses the ability
      on another creature.</p>
  links:
    classAssociations:
      - _index: 0
        level: 1
        name: Art of Deception (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.fJhe6hxe5sy7ipaI
      - _index: 1
        level: 10
        name: Assumption (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.R92HUUYKBGjvKCs3
      - _index: 2
        level: 4
        name: Concealed Thoughts (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.6C7tjwcoU8rAZRDX
      - _index: 3
        level: 8
        name: Death Attack (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Dgb1Hza9kOk45Xbu
      - _index: 4
        level: 5
        name: Elude Detection (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.MATG2mtsnXexDTF9
      - _index: 5
        level: 8
        name: Fool Casting (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.0YgTb3M1nBP1JgUK
      - _index: 6
        level: 2
        name: Glib Lie (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.J1QHd5YQmMKzcDFI
      - _index: 7
        level: 9
        name: Hidden Mind (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.jBoU0kvqez6OYU89
      - _index: 8
        level: 1
        name: Master of Disguise (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.dUPZnSwNwGtfOh5a
      - _index: 9
        level: 3
        name: Nonmagical Aura (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.jVp4nJstY8Pf4Apf
      - _index: 10
        level: 4
        name: Quick Change (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.EN32EMpk9zwreK4A
      - _index: 11
        level: 6
        name: Shift Alignment (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.qLswoTD7JmupA1id
      - _index: 12
        level: 5
        name: Slippery Mind (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.4mnIZfccFV4Gt3hY
      - _index: 13
        level: 1
        name: Sneak Attack (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.OdJ9RViJN1QsxxI0
      - _index: 14
        level: 3
        name: Superficial Knowledge (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.LGaYfwlBmmcL0C3T
      - _index: 15
        level: 2
        name: Mask Alignment (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.6pdttkHf2yzZORTg
  savingThrows:
    ref:
      value: high
    will:
      value: high
  skillsPerLevel: 6
  subType: prestige
  tag: masterSpy
type: class

