_id: 3Ipv1VivPHaJC1WU
_key: '!items!3Ipv1VivPHaJC1WU'
img: icons/commodities/flowers/clover-purple.webp
name: Bellflower Tiller
system:
  bab: med
  classSkills:
    acr: true
    blf: true
    dip: true
    dis: true
    esc: true
    hea: true
    int: true
    kge: true
    klo: true
    per: true
    ste: true
    sur: true
  description:
    value: >-
      <p>Perhaps no group has done more to sow the seeds of freedom in
      devil-haunted Cheliax than the Bellflower Network, and no agents of that
      network do or risk more than its dedicated Bellflower tillers. Charged
      with freeing slaves from the infernal nation’s many plantations and
      escorting them to the safety of Andoran and Rahadoum, Bellflower tillers
      are a constant thorn in the side of House Thrune and its diabolical
      minions. These elite liberators practice their craft all across Cheliax,
      from the darkened streets of Egorian and Westcrown to the wilds of the
      Whisperwood. Bellflower tillers employ farming euphemisms to mask their
      activities from unfriendly ears— referring to their secret hideouts as
      “barns,” the slaves they escort as their “crops,” and the secret paths
      they take as “rows.” Excessive contact between Bellflower tillers is
      discouraged by the Bellflower Network’s leadership, lest they risk
      exposing the organization to the authorities. Despite this, they often
      meet in secret to discuss which routes are safest to take, how much to
      bribe certain officials, and what tactics for organizing groups of slaves
      work best, and so improve chances of evading capture.</p><p>Nearly all
      Bellflower tillers are halfling rogues, though some particularly zealous
      members have levels in both rogue and inquisitors and savvy members might
      have levels in both rogue and ranger. Vigilantes are also a natural fit as
      Bellflower tillers. Non-halflings are usually admitted only after proving
      their allegiance to the anti-slavery movement and their goodwill toward
      halflings, but once they join the cause, they are incredibly valuable to
      the organization for the simple fact that their race does not immediately
      raise suspicion in areas where the Bellflower Network is active.</p><p>A
      Bellflower tiller’s duty relies in part on her ability to establish a
      believable and viable cover identity. Many pose as traveling merchants or
      tinkerers—occupations that give them the freedom to get where they need
      to, when they need to—and have alibis for their presence on the road if
      confronted by Chelish patrols. Other Bellflower tillers join mercenary
      bands or adventuring groups, using their organizational and survival
      skills to help their companions in exchange for some extra muscle should a
      particular slave breakout proceed poorly.</p><h2>Requirements</h2><p>To
      qualify to become a Bellflower tiller, a character must fulfill the
      following criteria.</p><p><strong>Alignment</strong>: Chaotic good. <br
      /><strong>Feats</strong>: Any two teamwork feats (<em>Pathfinder RPG
      Advanced Player’s Guide</em> 150). <br /><strong>Skills</strong>: Disguise
      5 ranks, Knowledge (local) 3 ranks, Stealth 5 ranks, Survival 5 ranks.<br
      /><strong> Special</strong>: Sneak attack +2d6 or any 2 vigilante
      talents.</p><h2>Class Skills</h2><p>The Bellflower Tiller's class skills
      are Acrobatics (Dex), Bluff (Cha), Diplomacy (Cha), Disguise (Cha), Escape
      Artist (Dex), Heal (Wis), Intimidate (Cha), Knowledge (geography) (Int),
      Knowledge (local) (Int), Perception (Wis), Sense Motive (Wis), Sleight of
      Hand (Dex), Stealth (Dex), and Survival (Wis).</p><p><strong>Skill Points
      at each Level</strong>: 6 + Int modifier.<br /><strong>Hit Die</strong>:
      d8.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:7.6898%"><p><strong>Level</strong></p></td><td
      style="width:13.7254%"><p><strong>Base Attack Bonus</strong></p></td><td
      style="width:7.82123%"><p><strong>Fort Save</strong></p></td><td
      style="width:7.07635%"><p><strong>Ref Save</strong></p></td><td
      style="width:7.63501%"><p><strong>Will Save</strong></p></td><td
      style="width:55.6797%"><p><strong>Special</strong></p></td></tr><tr><td
      style="width:7.6898%"><p>1st</p></td><td
      style="width:13.7254%"><p>+0</p></td><td
      style="width:7.82123%"><p>+0</p></td><td
      style="width:7.07635%"><p>+1</p></td><td
      style="width:7.63501%"><p>+0</p></td><td
      style="width:55.6797%"><p>Bellflower crop, swift sower +10
      ft.</p></td></tr><tr><td style="width:7.6898%"><p>2nd</p></td><td
      style="width:13.7254%"><p>+1</p></td><td
      style="width:7.82123%"><p>+1</p></td><td
      style="width:7.07635%"><p>+1</p></td><td
      style="width:7.63501%"><p>+1</p></td><td style="width:55.6797%"><p>Crop
      guardian, scarcrow +1, teamwork feat</p></td></tr><tr><td
      style="width:7.6898%"><p>3rd</p></td><td
      style="width:13.7254%"><p>+2</p></td><td
      style="width:7.82123%"><p>+1</p></td><td
      style="width:7.07635%"><p>+2</p></td><td
      style="width:7.63501%"><p>+1</p></td><td style="width:55.6797%"><p>Sneak
      attack +1d6</p></td></tr><tr><td style="width:7.6898%"><p>4th</p></td><td
      style="width:13.7254%"><p>+3</p></td><td
      style="width:7.82123%"><p>+1</p></td><td
      style="width:7.07635%"><p>+2</p></td><td
      style="width:7.63501%"><p>+1</p></td><td
      style="width:55.6797%"><p>Scarecrow +2</p></td></tr><tr><td
      style="width:7.6898%"><p>5th</p></td><td
      style="width:13.7254%"><p>+3</p></td><td
      style="width:7.82123%"><p>+2</p></td><td
      style="width:7.07635%"><p>+3</p></td><td
      style="width:7.63501%"><p>+2</p></td><td style="width:55.6797%"><p>1st
      favored barn</p></td></tr><tr><td style="width:7.6898%"><p>6th</p></td><td
      style="width:13.7254%"><p>+4</p></td><td
      style="width:7.82123%"><p>+2</p></td><td
      style="width:7.07635%"><p>+3</p></td><td
      style="width:7.63501%"><p>+2</p></td><td
      style="width:55.6797%"><p>Scarecrow +3, sneak attack +2d6, swift sower +20
      ft., teamwork feat</p></td></tr><tr><td
      style="width:7.6898%"><p>7th</p></td><td
      style="width:13.7254%"><p>+5</p></td><td
      style="width:7.82123%"><p>+2</p></td><td
      style="width:7.07635%"><p>+4</p></td><td
      style="width:7.63501%"><p>+2</p></td><td style="width:55.6797%"><p>2nd
      favored barn</p></td></tr><tr><td style="width:7.6898%"><p>8th</p></td><td
      style="width:13.7254%"><p>+6</p></td><td
      style="width:7.82123%"><p>+3</p></td><td
      style="width:7.07635%"><p>+4</p></td><td
      style="width:7.63501%"><p>+3</p></td><td
      style="width:55.6797%"><p>Scarecrow +4</p></td></tr><tr><td
      style="width:7.6898%"><p>9th</p></td><td
      style="width:13.7254%"><p>+6</p></td><td
      style="width:7.82123%"><p>+3</p></td><td
      style="width:7.07635%"><p>+5</p></td><td
      style="width:7.63501%"><p>+3</p></td><td style="width:55.6797%"><p>3rd
      favored barn, sneak attack +3d6</p></td></tr><tr><td
      style="width:7.6898%"><p>10th</p></td><td
      style="width:13.7254%"><p>+7</p></td><td
      style="width:7.82123%"><p>+3</p></td><td
      style="width:7.07635%"><p>+5</p></td><td
      style="width:7.63501%"><p>+3</p></td><td
      style="width:55.6797%"><p>Scarecrow +5, teamwork
      feat</p></td></tr></tbody></table><p><br />The following are class
      features of the Bellflower tiller prestige class.</p><p><strong>Bellflower
      Crop (Ex)</strong>: As a standard action, a Bellflower tiller can
      designate a number of allies up to 3 + her Charisma modifier as part of
      her Bellflower crop. Members of a crop must remain within 30 feet of the
      Bellflower tiller to gain any benefits from abilities that affect a
      Bellflower crop, regardless of the ability’s source (whether it’s a
      Bellflower harvester, irrigator, or tiller). If any leave this range, the
      Bellflower tiller must designate these allies again for them to be
      included in her crop. At 7th level, this range increases to 60
      feet.</p><p><strong>Swift Sower (Ex)</strong>: A Bellflower tiller’s base
      land speed is treated as being +10 feet faster when determining her
      overland travel speed while traveling long distances and while outside of
      combat. Members of her Bellflower crop can use either the Bellflower
      tiller’s overland travel speed or their own, whichever is better. At 6th
      level, the bonus to speed increases to +20 feet.</p><p><strong>Crop
      Guardian (Ex)</strong>: At 2nd level, whenever a Bellflower tiller uses
      the aid another action for a member of her Bellflower crop, she grants a
      +3 bonus instead of the normal +2. Levels in the Bellflower tiller
      prestige class stack with any vigilante levels she has when calculating
      the effects of the Bellflower harvester’s crop vigilance ability (see page
      42).</p><p><strong> Scarecrow (Ex)</strong>: At 2nd level, a
      Bellflower tiller receives a +1 morale bonus on attack and damage rolls
      against creatures that threaten attacks of opportunity against members of
      her Bellflower crop. At 4th level and every 2 levels thereafter, these
      bonuses increase by 1, to a maximum of +5 at 10th
      level.</p><p><strong>Teamwork Feat</strong>: At 2nd level, a Bellflower
      tiller gains a bonus feat in addition to those gained from normal
      advancement. This bonus feat must be a teamwork feat, and the Bellflower
      tiller must meet the prerequisites of the selected bonus feat. At 6th
      level and 10th level, the Bellflower tiller gains an additional bonus
      teamwork feat.</p><p><strong>Sneak Attack</strong>: At 3rd level, a
      Bellflower tiller gains a sneak attack that functions like the rogue class
      feature of the same name. The extra damage is 1d6 at 3rd level and
      increases by 1d6 every 3 levels thereafter. If a Bellflower tiller gets a
      sneak attack bonus from another source, the bonuses on damage
      stack.</p><p><strong>Favored Barn (Ex)</strong>: At 5th level, a
      Bellflower tiller grows familiar with and develops trusted contacts in a
      particular community. While inside the limits of this community, she gains
      a +2 bonus on initiative checks and Knowledge (local), Perception,
      Stealth, and Survival checks. Any ally designated as part of the
      Bellflower tiller’s crop receives a bonus on these skills equal to half
      the tiller’s bonus. A Bellflower tiller traveling through her favored
      community leaves no trail and cannot be tracked (although she can leave a
      trail if she so desires).</p><p>Provided that she isn’t in immediate
      danger (such as fleeing from pursuers right on her heels), a Bellflower
      tiller can always find a safe place for her and members of her crop to
      rest in her chosen community. At such a location, she and her allies are
      fed, clothed, and provided with basic medical attention (as if attended by
      a person with a Heal bonus of +10).</p><p>At 7th level and 9th level, the
      Bellflower tiller can select an additional community in which to receive
      these bonuses. Each time she selects a new community, the skill bonus and
      initiative bonus in any one chosen community (including the one just
      selected, if so desired) increases by 2.</p><p>For the purposes of this
      ability, a community can be any settlement of 100 or more individuals.
      Outlying farms, fields, and houses are considered part of a community.</p>
  links:
    classAssociations:
      - _index: 0
        level: 1
        name: Bellflower Crop (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.eZShxiJXWMbTTCXL
      - _index: 1
        level: 1
        name: Swift Sower (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.nFX6NXv1xQnNipTN
      - _index: 2
        level: 2
        name: Crop Guardian (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.o2rdmt2ZK2lrJFSH
      - _index: 3
        level: 2
        name: Scarecrow (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.IaCFDKoeuoyRIkTB
      - _index: 4
        level: 5
        name: Favored Barn (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.jpBI7of5g4YbY5zV
      - _index: 5
        level: 2
        name: Teamwork Feat
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.kD57bzGedm0ZIfdl
      - _index: 6
        level: 3
        name: Sneak Attack
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.buaq8IPRmUNO3PHm
  savingThrows:
    ref:
      value: high
  skillsPerLevel: 6
  sources:
    - id: PZO1138
      pages: '40'
    - id: PZO9249
      pages: '10'
  subType: prestige
  tag: bellflowerTiller
type: class

