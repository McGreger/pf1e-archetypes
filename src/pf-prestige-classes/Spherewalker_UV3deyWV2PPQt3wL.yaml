_id: UV3deyWV2PPQt3wL
_key: '!items!UV3deyWV2PPQt3wL'
img: icons/environment/wilderness/statue-hound-horned.webp
name: Spherewalker
system:
  bab: med
  classSkills:
    clm: true
    esc: true
    han: true
    hea: true
    kge: true
    prf: true
    rid: true
    sur: true
    swm: true
  description:
    value: >-
      <p><strong>Source</strong>: Pathfinder #2: The Skinsaw Murders pg. 73<br
      />A spherewalker is one who embraces the philosophy of Desna: travel far
      and wide, fight for your dreams, and indulge your desires. Spherewalkers
      explore extreme locales, dream fantastic dreams, and press their luck
      (good and bad) to make their lives interesting and noteworthy. They are
      known for founding cities, discovering lost civilizations, and going
      places where none have ever trod. They may choose to walk the distant
      paths alone or try to understand the mysteries of the mind and soul and
      become great leaders. Above all, spherewalkers seek adventure, with all
      the risks and rewards that such endeavors bring.</p><p>Spherewalkers
      are often multiclassed characters, as they tend to dabble in various
      classes to suit their adventuring needs. Their versatility adds extra
      depth and utility to combat-oriented characters (barbarians, fighters,
      rangers), their unusual magic appeals to spellcasters, and their special
      abilities make it easy for nonmagical characters to get out of tight
      spots.</p><h2>Requirements</h2><p>To qualify to become a spherewalker, a
      character must fulfill all of the following criteria:<br
      /><strong>Deity</strong>: Desna.<br /><strong>Base Attack Bonus</strong>:
      +5<br /><strong>Skills</strong>: 5 ranks in any two of the following
      skills: Climb, Knowledge (geography), Knowledge (nature), Knowledge
      (religion), Perform, Ride, Survival, or Swim.<br /><strong>Feats</strong>:
      Martial Weapon Proficentcy (starknife), one survivor feat (Great
      Fortitude, Iron Will, or Lightning Reflexes), one explorer feat
      (Acrobatic, Agile, Athletic, Endurance, Run, or Self-Sufficient).<br
      /><strong>Special</strong>: A spherewalker-to-be must have journeyed far
      in his lifetime, visiting two locations sacred to Desna that are at least
      200 miles apart.</p><h2>Class Skills</h2><p>The Spherewalker's class
      skills are Balance, Climb, Concentration, Escape Artist, Handle Animal,
      Heal, Jump, Knowledge (geography), Perform, Ride, Spot, Survival, Swim,
      and Tumble.</p><p><strong>Skill Points at each Level</strong>: 4 + Int
      modifier.<br /><strong>Hit Die</strong>: d8.</p><h2>Class
      Features</h2><table class="inner" style="width: 96.9314%;"><tbody><tr><td
      style="width: 8.45903%;"><strong>Level</strong></td><td style="width:
      14.8184%;"><strong>Base Attack Bonus</strong></td><td style="width:
      8.75233%;"><strong>Fort Save</strong></td><td style="width:
      7.82123%;"><strong>Ref Save</strong></td><td style="width:
      8.75233%;"><strong>Will Save</strong></td><td style="width:
      24.7672%;"><strong>Special</strong></td><td style="width:
      26.257%;"><strong>Spells Per Day</strong></td></tr><tr><td style="width:
      8.45903%;">1st</td><td style="width: 14.8184%;">+0</td><td style="width:
      8.75233%;">+2</td><td style="width: 7.82123%;">+2</td><td style="width:
      8.75233%;">+0</td><td style="width: 24.7672%;">Landmark,
      longstrider</td><td style="width: 26.257%;">+1 level of spellcasting
      class</td></tr><tr><td style="width: 8.45903%;">2nd</td><td style="width:
      14.8184%;">+1</td><td style="width: 8.75233%;">+3</td><td style="width:
      7.82123%;">+3</td><td style="width: 8.75233%;">+0</td><td style="width:
      24.7672%;">Efficient sleep, star slinger</td><td style="width:
      26.257%;">+1 level of spellcasting class</td></tr><tr><td style="width:
      8.45903%;">3rd</td><td style="width: 14.8184%;">+2</td><td style="width:
      8.75233%;">+3</td><td style="width: 7.82123%;">+3</td><td style="width:
      8.75233%;">+1</td><td style="width: 24.7672%;">Dream link</td><td
      style="width: 26.257%;">+1 level of spellcasting class</td></tr><tr><td
      style="width: 8.45903%;">4th</td><td style="width: 14.8184%;">+3</td><td
      style="width: 8.75233%;">+4</td><td style="width: 7.82123%;">+4</td><td
      style="width: 8.75233%;">+1</td><td style="width: 24.7672%;">Divine
      luck</td><td style="width: 26.257%;">+1 level of spellcasting
      class</td></tr><tr><td style="width: 8.45903%;">5th</td><td style="width:
      14.8184%;">+3</td><td style="width: 8.75233%;">+4</td><td style="width:
      7.82123%;">+4</td><td style="width: 8.75233%;">+1</td><td style="width:
      24.7672%;">Swarm form</td><td style="width: 26.257%;">+1 level of
      spellcasting
      class</td></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr></tbody></table><p><br
      />The following are class features of the spherewalker prestige class.<br
      /><br /><strong>Landmark (Su)</strong>: At 1st level, as a full-round
      action a spherewalker can create a mental landmark for her current
      location and thereafter take a standard action to note the general
      direction of that landmark. For example, if she sets her landmark in
      Magnimar and later travels to Korvosa, she can determine that Magnimar is
      roughly west of her current location. The landmark doesn&rsquo;t have to
      be a city or any kind of recognizable site&mdash;it could be in the middle
      of the ocean or even midair. This is a scrying effect. A landmark location
      is one category more familiar for the purpose of teleporting
      (&ldquo;studied carefully&rdquo; becomes &ldquo;very familiar&rdquo; and
      so on). A spherewalker can have one landmark per class level. She can
      discard an existing landmark as a standard action and does not need to be
      at that location to discard it. Spherewalkers can take a minute to share a
      landmark with another spherewalker, and often do so to arrange meetings in
      unusual places without the need for maps or directions. A spherewalker can
      only detect the location of landmarks on her current plane, though they do
      not vanish if she leaves the plane and she can reference them if she
      returns to that plane.</p><p><strong>Longstrider (Sp)</strong>: At 1st
      level, a spherewalker may use <em>longstrider</em> once per day. Her
      caster level is equal to her character level.</p><p><strong>Spells per
      Day</strong>: When a spherewalker gains a level, she gains new spells per
      day as if she had also gained a level in a spellcasting class she belonged
      to before she added the prestige class. She does not, however, gain any
      other benefit a character of that class would have gained. This
      essentially means that she adds the level of spherewalker to the level of
      whatever other spellcasting class she has, then determines spells per day
      and caster level accordingly.</p><p>If the character had more than one
      spellcasting class before she became a spherewalker, she must choose which
      class she adds each spherewalker level to for the purpose of determining
      spells per day.</p><p>If the spherewalker has no levels in a spellcasting
      class, she instead gains one 1st-level domain spell slot, which she may
      use to prepare spells from any of Desna&rsquo;s domains as if she were a
      cleric. Her caster level is equal to twice her class level. With each new
      spherewalker level, she gains a new spell slot for a spell level equal to
      her class level. A 5th-level spherewalker would thus have a spell slot for
      one domain spell from 1st to 5th level spells. The number of bonus spells
      and spell save DCs are set by the spherewalker&rsquo;s Wisdom
      score.</p><p><strong>Efficient Sleep (Su)</strong>: At 2nd level, a
      spherewalker gains a +4 sacred bonus to resist sleep effects and only
      needs 4 hours of sleep (or restful calm, for creatures that do not need
      actual sleep) instead of the normal 8 hours to become rested. Most
      spherewalkers prefer to sleep the normal amount, and if they use this
      ability they like to make up for it on later days by sleeping
      late.</p><p><strong>Star Slinger (Ex)</strong>: At 2nd level, any
      starknife a spherewalker uses in combat is treated as if it had the
      returning magic weapon special ability. Such weapons gain no additional
      enhancement bonuses or magical properties beyond this
      effect.</p><p><strong>Dream Link (Sp)</strong>: A 3rd-level spherewalker
      can form a mental bond with another willing intelligent creature as if
      using the <em>telepathic bond</em> spell. To do so, she and the target
      must spend at least 4 hours sleeping within 10 feet of each other. Either
      person in the link can end it as a standard action; otherwise it is
      permanent. If one person in the link dies, the other is stunned for 1
      round and staggered until she can rest for 10 minutes. A spherewalker can
      only maintain one such link at a time, though she may be the recipient of
      multiple links. Married spherewalkers often link themselves to their
      spouses.</p><p><strong>Divine Luck (Su)</strong>: At 4th level, a
      spherewalker can add a luck bonus equal to her class level on an attack
      roll, skill check, or saving throw a number of times per day equal to her
      Charisma modifier (minimum 1/day). She may add this bonus after she has
      rolled but before she knows if the unmodified result is a success or
      failure.</p><p><strong>Swarm Form (Su)</strong>: At 5th level, a
      spherewalker gains the ability to transform into a swarm of Diminutive
      butterflies. In swarm form, she has a space of 10 feet (roughly filling
      the entire area) but can shape this space to fill four contiguous squares
      (such as a 5-foot-by- 20-foot line, an L-shaped cloud, and so on) and can
      squeeze through any space large enough to contain one of her component
      forms. The swarm can fly at a speed of 40 feet (good). Like any swarm, it
      can occupy the same space as another creature regardless of its
      size.</p><p>Any creature that begins its turn sharing a space with a swarm
      must succeed on a Fortitude save (DC 10 + spherewalker level +
      Constitution bonus) or be nauseated for 1 round. Unlike most swarms, a
      spherewalker in swarm form does not do swarm damage to creatures
      she&rsquo;s swarming over. The swarm form is immune to weapon damage but
      is vulnerable to mundane fire attacks (torches, alchemical fire, burning
      oil, and so on), and energy attacks from weapon (such as flaming and
      frost) deal full damage even though the basic weapon damage has no effect.
      Although a swarm cannot make attacks, the spherewalker can cast spells as
      normal while in swarm form (although spells with material components could
      prove difficult).</p><p>A spherewalker can use this ability a number of
      times per day equal to her class level and remain in this form for up to
      one minute. While in swarm form, she may expend one use of this ability as
      a free action in order to remain in swarm form for an additional minute
      (rather than changing back to her normal form and activating it again).
      Changing back to her natural form before the effect ends is a standard
      action.</p>
  links:
    classAssociations:
      - _index: 0
        level: 4
        name: Divine Luck (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.su4BFzxn5pSyDFLt
      - _index: 1
        level: 3
        name: Dream Link (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.lzwZGyJnFzvlHXAT
      - _index: 2
        level: 2
        name: Efficient Sleep (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.JHMjcL8WElA3MopJ
      - _index: 3
        level: 1
        name: Landmark (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.y60Fl30OTdBQmf41
      - _index: 4
        level: 1
        name: Longstrider (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.wlfWioBaK9Ihu0uk
      - _index: 5
        level: 2
        name: Star Slinger (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.pDnyX8JPF9SeanD6
      - _index: 6
        level: 5
        name: Swarm Form (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.qhDs2AuUaAegMUUO
  savingThrows:
    fort:
      value: high
    ref:
      value: high
  skillsPerLevel: 4
  subType: prestige
  tag: spherewalker
type: class

