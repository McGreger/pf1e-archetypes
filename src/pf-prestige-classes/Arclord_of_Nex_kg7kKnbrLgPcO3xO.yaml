_id: kg7kKnbrLgPcO3xO
_key: '!items!kg7kKnbrLgPcO3xO'
img: systems/pf1/icons/skills/yellow_29.jpg
name: Arclord of Nex
system:
  classSkills:
    dip: true
    lin: true
    umd: true
  description:
    value: >-
      <p>The Arclords of Nex trace their lineage to the personal retinue of the
      archmage Nex, and though the demiurge has been missing for over four
      millennia, the Arclords still seek to enact his will. Whenever possible,
      they follow in the literal footsteps of Nex by looking to the
      wizard-king’s diary entries, workbooks, and scrolls (with such resources
      often being of questionable legitimacy). Seizing political power in the
      years immediately after his disappearance, the Arclords reinitiated
      hostilities with rival Geb while instituting laws and social policies
      derived from hoarded scraps of Nex’s personal correspondence and journals.
      This behavior led to their ouster and exile from Nex.</p><p>In the
      centuries since, most Arclords have pursued less radical methods of
      carrying out Nex’s will. Arclords pioneered and refined the practical
      application of magical constructs, conjurations, and education, founding
      the legendary Arcanamirium in Absalom. In Quantium, the Arclords’ ancient
      exile is long since forgotten. The Arclords’ goal is as subtle as it is
      simple—to make themselves and their magic (and, in a broader sense, all
      magic) an indispensable and irreplaceable foundation of communities and
      societies across the face of Golarion. In this way, they have achieved
      positions of inf luence and leadership that eluded them when they tried to
      seize power directly, and their voice is loud and persuasive in courts
      throughout the Inner Sea.</p><h2>Requirements</h2><p>To qualify to become
      an Arclord of Nex, a character must fulfill all of the following
      criteria.</p><p><strong>Feats</strong>: Craft Construct, Craft Wondrous
      Item, Eye of the Arclord (<em>The Inner Sea World Guide </em>286).<br
      /><strong>Skills</strong>: Diplomacy 5 ranks, Knowledge (arcana) 5 ranks,
      Knowledge (engineering) 5 ranks, Spellcraft 5 ranks.<br
      /><strong>Special</strong>: You must be able to cast <em>arcane
      sight</em>, and have at least two spells from every school of magic
      scribed in your spellbook.<br /><strong>Special</strong>: Arcane school
      class feature. You must have the ability to use hand of the apprentice or
      a different arcane school power that you can use a number of times per day
      equal to 3 + your Intelligence modifier. For Arclord of Nex abilities that
      require you to expend a number of uses of hand of the apprentice, if you
      can’t use hand of the apprentice you can substitute an equivalent arcane
      school power you can use a number of times per day equal to 3 + your
      Intelligence modifier. If you do, you must expend one additional
      use.</p><h2>Class Skills</h2><p>The Arclord of Nex's class skills are
      Diplomacy (Cha), Linguistics (Int), Sense Motive (Wis), and Use Magic
      Device (Cha).</p><p><strong>Skill Points at each Level</strong>: 2 + Int
      modifier.<br /><strong>Hit Die</strong>: d6.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:10.9591%"><strong>Level</strong></td><td
      style="width:11.0149%"><strong>Base Attack Bonus</strong></td><td
      style="width:8.19367%"><strong>Fort Save</strong></td><td
      style="width:7.26257%"><strong>Ref Save</strong></td><td
      style="width:8.00745%"><strong>Will Save</strong></td><td
      style="width:23.6499%"><strong>Special</strong></td><td
      style="width:30.54%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:10.9591%">1st</td><td style="width:11.0149%">+0</td><td
      style="width:8.19367%">+0</td><td style="width:7.26257%">+0</td><td
      style="width:8.00745%">+1</td><td style="width:23.6499%">Mystic pedagogue,
      third eye</td><td style="width:30.54%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:10.9591%">2nd</td><td
      style="width:11.0149%">+1</td><td style="width:8.19367%">+1</td><td
      style="width:7.26257%">+1</td><td style="width:8.00745%">+1</td><td
      style="width:23.6499%">Artificer’s touch</td><td style="width:30.54%">+1
      level of arcane spellcasting class</td></tr><tr><td
      style="width:10.9591%">3rd</td><td style="width:11.0149%">+1</td><td
      style="width:8.19367%">+1</td><td style="width:7.26257%">+1</td><td
      style="width:8.00745%">+2</td><td style="width:23.6499%">Scholiast
      1</td><td style="width:30.54%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:10.9591%">4th</td><td
      style="width:11.0149%">+2</td><td style="width:8.19367%">+1</td><td
      style="width:7.26257%">+1</td><td style="width:8.00745%">+2</td><td
      style="width:23.6499%">Arcane architect</td><td style="width:30.54%">+1
      level of arcane spellcasting class</td></tr><tr><td
      style="width:10.9591%">5th</td><td style="width:11.0149%">+2</td><td
      style="width:8.19367%">+2</td><td style="width:7.26257%">+2</td><td
      style="width:8.00745%">+3</td><td style="width:23.6499%">Improved third
      eye</td><td style="width:30.54%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:10.9591%">6th</td><td
      style="width:11.0149%">+3</td><td style="width:8.19367%">+2</td><td
      style="width:7.26257%">+2</td><td style="width:8.00745%">+3</td><td
      style="width:23.6499%">Scholiast 2</td><td style="width:30.54%">+1 level
      of arcane spellcasting class</td></tr><tr><td
      style="width:10.9591%">7th</td><td style="width:11.0149%">+3</td><td
      style="width:8.19367%">+2</td><td style="width:7.26257%">+2</td><td
      style="width:8.00745%">+4</td><td style="width:23.6499%">Extradimensional
      extension</td><td style="width:30.54%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:10.9591%">8th</td><td
      style="width:11.0149%">+4</td><td style="width:8.19367%">+3</td><td
      style="width:7.26257%">+3</td><td style="width:8.00745%">+4</td><td
      style="width:23.6499%">Call for aid</td><td style="width:30.54%">+1 level
      of arcane spellcasting class</td></tr><tr><td
      style="width:10.9591%">9th</td><td style="width:11.0149%">+4</td><td
      style="width:8.19367%">+3</td><td style="width:7.26257%">+3</td><td
      style="width:8.00745%">+5</td><td style="width:23.6499%">Scholiast
      3</td><td style="width:30.54%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:10.9591%">10th</td><td
      style="width:11.0149%">+5</td><td style="width:8.19367%">+3</td><td
      style="width:7.26257%">+3</td><td style="width:8.00745%">+5</td><td
      style="width:23.6499%">Greater third eye</td><td style="width:30.54%">+1
      level of arcane spellcasting class</td></tr></tbody></table><p><br />The
      following are class features of the Arclord of Nex prestige
      class.</p><p><strong>Weapon and Armor Proficiency</strong>: An Arclord of
      Nex gains no additional weapon or armor
      proficiencies.</p><p><strong>Mystic Pedagogue (Ex)</strong>: An Arclord of
      Nex adds a bonus equal to 1/2 his class level on Spellcraft checks to
      learn a wizard spell or craft a magical item.</p><p><strong>Third Eye
      (Su)</strong>: An Arclord of Nex can use his Eye of the Arclord feat one
      additional time per day, plus one additional time per day for every two
      levels beyond 1st (three times per day at 3rd level, and so on). In
      addition, while the eye is open, the Arclord can use the aid another
      action to grant an adjacent wizard a +1 bonus to her caster level and a +2
      circumstance bonus on concentration checks for the next wizard spell she
      casts before the beginning of the Arclord’s next turn.<br /><br
      /><strong>Artificer’s Touch (Sp)</strong>: At 2nd level, an Arclord of Nex
      can expend one use of his hand of the apprentice to use the Artifice
      domain’s artificer’s touch power, using his caster level as his cleric
      level.</p><p><strong>Scholiast (Sp)</strong>: At 3rd level, an Arclord of
      Nex can access the wizard school powers of another school of magic. He
      chooses a single arcane school spell-like ability that has a number of
      uses per day equal to 3 + his Intelligence modifier. Henceforth, he may
      use that ability (with a wizard level equal to his caster level) by
      expending two uses of his hand of the apprentice ability. A specialist
      wizard needs to expend two uses of his equivalent ability if using
      scholiast for hand of the apprentice, but still has to expend one
      additional daily use (for a total of three) to use it for a power of a
      different school.</p><p>At 6th level and again at 9th level, an Arclord of
      Nex can choose an additional arcane school spell-like ability to use with
      this ability.</p><p><strong>Arcane Architect (Ex)</strong>: At 4th level,
      an Arclord of Nex can create constructs with the Craft Construct feat in
      only half the time it would normally take. In addition, he can apply the
      Extend Spell and/or Widen Spell metamagic feats to conjuration (creation)
      spells he prepares, even if he doesn’t possess those feats. If he does
      possess them, the level increase for applying Extend Spell to a
      conjuration (creation) spell is eliminated, and the level increase for
      applying Widen Spell to a conjuration (creation) spell is reduced to
      2.</p><p><strong>Improved Third Eye (Sp)</strong>: At 5th level, an
      Arclord can activate his Eye of the Arclord feat as a move action. An
      Arclord of Nex can expend two uses of his hand of the apprentice ability
      to add the effect of <em>arcane sight </em>or <em>see invisibility </em>to
      the benefits of his Eye of the Arclord for 1 minute (or until the duration
      of the Eye ends, whichever is sooner). He can add both effects by
      expending four uses of this ability.</p><p><strong>Extradimensional
      Extension (Su)</strong>: At 6th level, the duration of any spell cast by
      an Arclord of Nex that creates an extradimensional space (or demiplane) or
      moves creatures or objects through the Ethereal Plane is doubled as though
      affected by the Extend Spell feat, without altering the level of the
      spell. Applications of Extend Spell do not stack with this
      ability.</p><p><strong>Call for Aid (Su)</strong>: At 8th level, as a
      standard action, an Arclord of Nex can sacrifice a prepared spell of 2nd
      level or higher to call a creature bound to him, including an animal
      companion, eidolon, familiar, personally controlled construct or undead
      creature, creature telepathically linked to him (including by
      <em>telepathic bond</em>, <em>dominate person</em>, or <em>dominate
      monster</em>), or an outsider bound with a <em>planar binding</em> spell.
      The named creature appears adjacent to the Arclord, or in the nearest open
      space if all adjacent squares are occupied. This ability functions like
      <em>greater teleport</em>, except the range limit is determined by the
      level of the sacrificed spell: short (2nd–3rd), medium (4th–5th), long
      (6th–7th), unlimited (same plane) (8th), or unlimited (any plane)
      (9th).</p><p><strong>Greater Third Eye (Sp)</strong>: At 10th level, an
      Arclord can activate his Eye of the Arclord as a swift action. In
      addition, he can expend four uses of his hand of the apprentice while
      opening his third eye to gain <em>greater arcane sight </em>or <em>true
      seeing </em>until the beginning of his next turn. He gains both effects if
      he expends eight uses of this ability.</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - _index: 0
        level: 4
        name: Arcane Architect (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.kAyBtmoyrqzJVxeQ
      - _index: 1
        level: 2
        name: Artificer's Touch (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.IMtR2qFe9q6kMPU1
      - _index: 2
        level: 8
        name: Call for Aid (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.AmlfJBXGQXCWXzQN
      - _index: 3
        level: 6
        name: Extradimensional Extension (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.srgFtHnmOS75NcX1
      - _index: 4
        level: 10
        name: Greater Third Eye (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.sLIlCIHYSEBpN694
      - _index: 5
        level: 5
        name: Improved Third Eye (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.pTEskaam7rtY0xIA
      - _index: 6
        level: 1
        name: Mystic Pedagogue (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.DMlr95khD8K18jE1
      - _index: 7
        level: 1
        name: Third Eye (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.NxKDrK81s0pokh0U
      - _index: 8
        level: 3
        name: Scholiast (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.XkVXOND8Q1nta92s
  savingThrows:
    will:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO9249
      pages: '6'
  subType: prestige
  tag: arclordOfNex
type: class

