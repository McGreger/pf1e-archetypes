_id: 7D5R7zpZ7EA6R1N9
_key: '!items!7D5R7zpZ7EA6R1N9'
img: icons/sundries/gaming/playing-cards-grey.webp
name: Harrower
system:
  classSkills:
    blf: true
    crf: true
    dip: true
    kar: true
    klo: true
    kpl: true
    kre: true
    prf: true
    pro: true
    spl: true
    umd: true
  description:
    value: >-
      <p>One of the most mysterious and mystical Varisian traditions, and
      certainly one that has captured the imaginations and curiosity of many of
      the Inner Sea’s peoples, is the harrowing. Using a special deck of cards
      known as a Harrow deck, one can perform a harrowing by laying the cards
      out in a three-by-three grid that reveals secrets of the target’s past,
      present, and future.</p><p>Whether or not the revelations and divinations
      of a typical harrowing are trustworthy, none can deny that the strange
      powers wielded by the harrower are real. The harrower uses the ancient art
      of fortune-telling to harness destiny and thus augment her spellcasting
      abilities, infusing them with power by drawing cards from her Harrow deck
      and letting fate decide what elements of her magic need
      augmentation.</p><p>Several of the harrower’s powers require the drawing
      of cards from a Harrow deck. You can also simulate a draw from such a deck
      of cards as detailed on page 293, under the equipment entry for Harrow
      decks. For most of the harrower’s class features, you need only roll 1d6
      to determine the suit of each card drawn for a Harrow casting. Only the
      blessing of the Harrow and spirit deck class features pay attention to a
      card’s alignment.</p><h2>Requirements</h2><p>To qualify to become a
      harrower, a character must fulfill all the following
      criteria.</p><p><strong>Alignment</strong>: Must be chaotic, evil, good,
      or lawful (cannot be true neutral). Most harrowers represent alignment
      extremes: lawful good, chaotic good, lawful evil, or chaotic evil.<br
      /><strong>Feats</strong>: Harrowed.<br /><strong>Skills</strong>:
      Knowledge (arcana or religion) 5 ranks, Perform (any) 5 ranks.<br
      /><strong>Special</strong>: Ability to cast 3rd-level spells. Must be able
      to cast at least three divination spells.<br /><strong>Special</strong>:
      Must own a Harrow deck.</p><h2>Class Skills</h2><p>The Harrower's class
      skills are Bluff (Cha), Craft (Int), Diplomacy (Cha), Knowledge (arcana)
      (Int), Knowledge (local) (Int), Knowledge (planes) (Int), Knowledge
      (religion) (Int), Perform (Cha), Profession (Wis), Spellcraft (Int), and
      Use Magic Device (Cha).</p><p><strong>Skill Points at each Level</strong>:
      2 + Int modifier.<br /><strong>Hit Die</strong>: d6.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:6.92057%"><p><strong>Level</strong></p></td><td
      style="width:14.8672%"><p><strong>Base Attack Bonus</strong></p></td><td
      style="width:8.00745%"><p><strong>Fort Save</strong></p></td><td
      style="width:7.07635%"><p><strong>Ref Save</strong></p></td><td
      style="width:7.82123%"><p><strong>Will Save</strong></p></td><td
      style="width:31.0987%"><p><strong>Special</strong></p></td><td
      style="width:23.8361%"><p><strong>Spells Per
      Day</strong></p></td></tr><tr><td
      style="width:6.92057%"><p>1st</p></td><td
      style="width:14.8672%"><p>+0</p></td><td
      style="width:8.00745%"><p>+0</p></td><td
      style="width:7.07635%"><p>+0</p></td><td
      style="width:7.82123%"><p>+1</p></td><td
      style="width:31.0987%"><p>Blessing of the Harrow</p></td><td
      style="width:23.8361%"><p>+1 level of spellcasting
      class</p></td></tr><tr><td style="width:6.92057%"><p>2nd</p></td><td
      style="width:14.8672%"><p>+1</p></td><td
      style="width:8.00745%"><p>+1</p></td><td
      style="width:7.07635%"><p>+1</p></td><td
      style="width:7.82123%"><p>+1</p></td><td style="width:31.0987%"><p>Harrow
      casting, Tower of Intelligence</p></td><td style="width:23.8361%"><p>+1
      level of spellcasting class</p></td></tr><tr><td
      style="width:6.92057%"><p>3rd</p></td><td
      style="width:14.8672%"><p>+1</p></td><td
      style="width:8.00745%"><p>+1</p></td><td
      style="width:7.07635%"><p>+1</p></td><td
      style="width:7.82123%"><p>+2</p></td><td style="width:31.0987%"><p>Tower
      of Strength</p></td><td style="width:23.8361%"><p>+1 level of spellcasting
      class</p></td></tr><tr><td style="width:6.92057%"><p>4th</p></td><td
      style="width:14.8672%"><p>+2</p></td><td
      style="width:8.00745%"><p>+1</p></td><td
      style="width:7.07635%"><p>+1</p></td><td
      style="width:7.82123%"><p>+2</p></td><td style="width:31.0987%"><p>Tower
      of Charisma</p></td><td style="width:23.8361%"><p>+1 level of spellcasting
      class</p></td></tr><tr><td style="width:6.92057%"><p>5th</p></td><td
      style="width:14.8672%"><p>+2</p></td><td
      style="width:8.00745%"><p>+2</p></td><td
      style="width:7.07635%"><p>+2</p></td><td
      style="width:7.82123%"><p>+3</p></td><td style="width:31.0987%"><p>Spirit
      deck</p></td><td style="width:23.8361%"><p>+1 level of spellcasting
      class</p></td></tr><tr><td style="width:6.92057%"><p>6th</p></td><td
      style="width:14.8672%"><p>+3</p></td><td
      style="width:8.00745%"><p>+2</p></td><td
      style="width:7.07635%"><p>+2</p></td><td
      style="width:7.82123%"><p>+3</p></td><td
      style="width:31.0987%"><p>Divination</p></td><td
      style="width:23.8361%"><p>+1 level of spellcasting
      class</p></td></tr><tr><td style="width:6.92057%"><p>7th</p></td><td
      style="width:14.8672%"><p>+3</p></td><td
      style="width:8.00745%"><p>+2</p></td><td
      style="width:7.07635%"><p>+2</p></td><td
      style="width:7.82123%"><p>+4</p></td><td style="width:31.0987%"><p>Tower
      of Constitution</p></td><td style="width:23.8361%"><p>+1 level of
      spellcasting class</p></td></tr><tr><td
      style="width:6.92057%"><p>8th</p></td><td
      style="width:14.8672%"><p>+4</p></td><td
      style="width:8.00745%"><p>+3</p></td><td
      style="width:7.07635%"><p>+3</p></td><td
      style="width:7.82123%"><p>+4</p></td><td style="width:31.0987%"><p>Tower
      of Dexterity</p></td><td style="width:23.8361%"><p>+1 level of
      spellcasting class</p></td></tr><tr><td
      style="width:6.92057%"><p>9th</p></td><td
      style="width:14.8672%"><p>+4</p></td><td
      style="width:8.00745%"><p>+3</p></td><td
      style="width:7.07635%"><p>+3</p></td><td
      style="width:7.82123%"><p>+5</p></td><td style="width:31.0987%"><p>Tower
      of Wisdom</p></td><td style="width:23.8361%"><p>+1 level of spellcasting
      class</p></td></tr><tr><td style="width:6.92057%"><p>10th</p></td><td
      style="width:14.8672%"><p>+5</p></td><td
      style="width:8.00745%"><p>+3</p></td><td
      style="width:7.07635%"><p>+3</p></td><td
      style="width:7.82123%"><p>+5</p></td><td style="width:31.0987%"><p>Reading
      the signs</p></td><td style="width:23.8361%"><p>+1 level of spellcasting
      class</p></td></tr></tbody></table><p><br />The following are class
      features of the harrower.</p><p><strong>Weapon and Armor
      Proficiency</strong>: A harrower gains no additional weapon or armor
      proficiencies.</p><p><strong>Spells</strong>: When a harrower gains a
      level, she gains new spells per day as if she had also gained a level in a
      spellcasting class she belonged to before she added the prestige class.
      She does not, however, gain any other benefits a character of that class
      would have gained. This essentially means that she adds the level of
      harrower to the level of whatever other spellcasting class she has. If the
      character had more than one spellcasting class before she became a
      harrower, she must choose which class she adds each harrower level to for
      the purposes of determining spells per day.</p><p>A harrower adds the
      <em>harrowing</em> spell (see page 295) to all of her spell lists as a
      3rd-level spell if it is not already on a spell
      list.</p><p><strong>Blessing of the Harrow (Su)</strong>: Once per day, a
      harrower may perform a harrowing for herself and all allies within 20 feet
      of her. This harrowing takes 10 minutes, and allies to be affected by it
      must remain within 20 feet of the harrower for the entire time. At the
      conclusion of the harrowing, count up the number of cards from each suit
      that were used in the reading. This harrowing provides a bonus based upon
      the suit with the most cards showing. In case of a tie, choose one suit.
      The bonus lasts for 24 hours. The suits grant insight bonuses as follows.
      Strength: +1 on attack rolls; Dexterity: +1 to AC; Constitution: +1 on
      weapon damage rolls; Intelligence: +1 on all skill checks; Wisdom: +1 on
      all saving throws; Charisma: +1 on caster level and concentration
      checks.</p><p><strong>Harrow Casting (Su)</strong>: Beginning at 2nd
      level, a harrower may, as she casts a spell, draw three cards from her
      Harrow deck. This adds both a somatic component (if the spell does not
      already have one) and a focus component (the Harrow deck) to the spell,
      but does not increase the spell’s casting time. Depending on the
      harrower’s level, the cards she draws might change the parameters of her
      spell or grant her some other benefit, as described in each tower ability.
      The harrower gains all of the different tower abilities available to her.
      If she draws cards that she has not yet gained the use of, those cards
      provide no benefit. Each card the harrower draws that exactly matches her
      alignment counts as two cards of that suit. A spell may not be affected by
      both Harrow casting and a metamagic feat. The harrower may use this
      ability a number of times per day equal to her class level.</p><table
      class="inner"><tbody><tr><td><p><strong>Alignment</strong></p></td><td><p><strong>Opposite</strong></p></td><td><p><strong>Partial
      Matches</strong></p></td></tr><tr><td><p>LG</p></td><td><p>CE</p></td><td><p>CG,
      LE, LN, NG</p></td></tr><tr><td><p>NG</p></td><td><p>NE</p></td><td><p>CG,
      LG, N</p></td></tr><tr><td><p>CG</p></td><td><p>LE</p></td><td><p>CE, CN,
      LG, NG</p></td></tr><tr><td><p>LN</p></td><td><p>CN</p></td><td><p>LE, LG,
      N</p></td></tr><tr><td><p>N</p></td><td><p>—</p></td><td><p>—</p></td></tr><tr><td><p>CN</p></td><td><p>LN</p></td><td><p>CE,
      CG, N</p></td></tr><tr><td><p>LE</p></td><td><p>CG</p></td><td><p>CE, LG,
      LN, NE</p></td></tr><tr><td><p>NE</p></td><td><p>NG</p></td><td><p>CE, LE,
      N</p></td></tr><tr><td><p>CE</p></td><td><p>LG</p></td><td><p>CG, CN, LE,
      NE</p></td></tr></tbody></table><p><strong>Tower of Intelligence
      (Su)</strong>: Beginning at 2nd level, whenever a harrower uses her Harrow
      casting ability, for each card she draws from the suit of Intelligence,
      she gains a +1 bonus on caster level checks made to penetrate Spell
      Resistance.</p><p><strong>Tower of Strength (Su)</strong>: Beginning at
      3rd level, whenever a harrower uses her Harrow casting ability to augment
      a spell that deals damage to hit points, the spell deals +1 point of
      damage per die for each card from the suit of Strength she
      draws.</p><p><strong>Tower of Charisma (Su)</strong>: Beginning at 4th
      level, whenever a harrower uses her Harrow casting ability, for each card
      she draws from the suit of Charisma, the save DC of the spell increases by
      +1.</p><p><strong>Spirit Deck (Su)</strong>: A 5th-level harrower may, as
      a standard action, summon a shimmering, translucent Harrow deck made of
      force that f lies through the air and engulfs a target within 30 feet in a
      whirling cloud of knife-edged cards. The harrower then draws a number of
      Harrow cards equal to her harrower level from her personal Harrow deck,
      and the spirit deck deals damage based on the number of drawn cards that
      match her alignment, as shown on the chart to the right. Each exact match
      deals 5 points of damage, each partial match deals 3 points, each
      non-matched card deals 1 point, and each opposite match deals 0 points.
      The harrower may use this ability a number of times per day equal to 1 +
      her Charisma modifier (minimum 1/day).</p><p><strong>Divination
      (Sp)</strong>: A 6th-level harrower gains the ability to cast
      <em>divination</em> once per day as a spell-like ability. Her caster level
      equals her character level.</p><p><strong>Tower of Constitution
      (Ex)</strong>: Beginning at 7th level, whenever a harrower uses her Harrow
      casting ability, for each card she draws from the suit of Constitution,
      she heals 1d6 points of damage.</p><p><strong>Tower of Dexterity
      (Su)</strong>: At 8th level, whenever a harrower uses her Harrow casting
      ability, for each card she draws from the suit of Dexterity, she gains a
      +1 bonus on Ref lex saves and to AC until the beginning of her next
      turn.</p><p><strong>Tower of Wisdom (Su)</strong>: At 9th level, whenever
      a harrower uses her Harrow casting ability, for each card she draws from
      the suit of Wisdom, she increases the spell’s effective caster level by
      +1.</p><p><strong>Reading the Signs (Ex)</strong>: When a 10th-level
      harrower draws cards from any deck of cards in order to either activate a
      class ability (such as Harrow casting) or to activate a magic item
      function, she may draw an extra card and choose one to ignore and shuff le
      back into the deck. The harrower may not use this ability when performing
      a harrowing, but may use it when performing her Harrow casting or spirit
      deck abilities. She can also use this ability when drawing cards from
      magical decks, such as a <em>deck of many things</em>. She may use this
      ability at will, but must wait 1d4 rounds between each use.</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - _index: 0
        level: 1
        name: Blessing of the Harrow (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.l2ScM18JPcHj6E62
      - _index: 1
        level: 6
        name: Divination (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.nB3FkWMDt8b75D3p
      - _index: 2
        level: 2
        name: Harrow Casting (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.NgGQ80uiwIbckeUh
      - _index: 3
        level: 10
        name: Reading the Signs (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.KPOG1CMjk7M5bbCv
      - _index: 4
        level: 5
        name: Spirit Deck (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.IQGNLG7hseEG8WG0
      - _index: 5
        level: 4
        name: Tower of Charisma (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.UhlEsz4D9utIldtM
      - _index: 6
        level: 7
        name: Tower of Constitution (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.xBeVV3IkgU1hjpLY
      - _index: 7
        level: 8
        name: Tower of Dexterity (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.cXTyqKoPmY0k7wUW
      - _index: 8
        level: 2
        name: Tower of Intelligence (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.gwZJB6sOfwrkSpUD
      - _index: 9
        level: 3
        name: Tower of Strength (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.FziD27RaGroNKWAk
      - _index: 10
        level: 9
        name: Tower of Wisdom (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.oNz3KmMw1AmkmMwh
  savingThrows:
    will:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO9226
      pages: '276'
    - id: PZO1111
      pages: '224'
  subType: prestige
  tag: harrower
type: class

