_id: ezdQLZzQybE1KROM
_key: '!items!ezdQLZzQybE1KROM'
img: systems/pf1/icons/skills/yellow_14.jpg
name: Sentinel
system:
  armorProf:
    value:
      - lgt
      - med
      - hvy
      - shl
  bab: high
  classSkills:
    clm: true
    crf: true
    han: true
    int: true
    kre: true
    per: true
    pro: true
    rid: true
    sur: true
    swm: true
  description:
    value: >-
      <p>Every major deity boasts champions of some type, whether they are
      paladins sworn to Iomedae or barbarians who channel their destructive rage
      from Rovagug. The sentinel is a warrior who receives special powers in
      exchange for his service to a deity, and often serves as a guard for the
      deity’s clergy. Some undergo official training, others experience visions
      leading them to their path, and some simply make the decision to pledge
      their lives to upholding their faith.</p><p>Sentinels frequently clash
      with enemies of their deity or champions of rival faiths, and occasionally
      work with other sentinels to undertake great quests or overcome
      challenging obstacles. Some inspire unique warrior orders dedicated to
      their faith, and legendary champions of the faith are often
      sentinels.</p><h2>Requirements</h2><p>To qualify to become a sentinel, a
      character must fulfill all of the following
      criteria.</p><p><strong>Alignment</strong>: Within one step of chosen
      deity.<br /><strong>Deity</strong>: Must worship a single, specific
      deity.<br /><strong>Feats</strong>: Deific Obedience, Weapon Focus
      (deity’s favored weapon).<br /><strong>Special</strong>: Base attack bonus
      +5 or higher.</p><h2>Class Skills</h2><p>The Sentinel's class skills are
      Climb (Str), Craft (Int), Handle Animal (Cha), Intimidate (Cha), Knowledge
      (religion) (Int), Perception (Wis), Profession (Wis), Ride (Dex), Survival
      (Wis), and Swim (Str).</p><p><strong>Skill Points at each Level</strong>:
      2 + Int modifier.<br /><strong>Hit Die</strong>: d10.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:7.6898%"><strong>Level</strong></td><td
      style="width:19.312%"><strong>Base Attack Bonus</strong></td><td
      style="width:10.8007%"><strong>Fort Save</strong></td><td
      style="width:9.68343%"><strong>Ref Save</strong></td><td
      style="width:10.6145%"><strong>Will Save</strong></td><td
      style="width:41.527%"><strong>Special</strong></td></tr><tr><td
      style="width:7.6898%">1st</td><td style="width:19.312%">+1</td><td
      style="width:10.8007%">+1</td><td style="width:9.68343%">+0</td><td
      style="width:10.6145%">+0</td><td style="width:41.527%">Obedience,
      symbolic weapon +1</td></tr><tr><td style="width:7.6898%">2nd</td><td
      style="width:19.312%">+2</td><td style="width:10.8007%">+1</td><td
      style="width:9.68343%">+1</td><td style="width:10.6145%">+1</td><td
      style="width:41.527%">Bonus feat</td></tr><tr><td
      style="width:7.6898%">3rd</td><td style="width:19.312%">+3</td><td
      style="width:10.8007%">+2</td><td style="width:9.68343%">+1</td><td
      style="width:10.6145%">+1</td><td style="width:41.527%">Divine boon 1,
      symbolic weapon +2</td></tr><tr><td style="width:7.6898%">4th</td><td
      style="width:19.312%">+4</td><td style="width:10.8007%">+2</td><td
      style="width:9.68343%">+1</td><td style="width:10.6145%">+1</td><td
      style="width:41.527%">Divine quickness +2</td></tr><tr><td
      style="width:7.6898%">5th</td><td style="width:19.312%">+5</td><td
      style="width:10.8007%">+3</td><td style="width:9.68343%">+2</td><td
      style="width:10.6145%">+2</td><td style="width:41.527%">Aligned strike,
      stalwart</td></tr><tr><td style="width:7.6898%">6th</td><td
      style="width:19.312%">+6</td><td style="width:10.8007%">+3</td><td
      style="width:9.68343%">+2</td><td style="width:10.6145%">+2</td><td
      style="width:41.527%">Divine boon 2, symbolic weapon +3</td></tr><tr><td
      style="width:7.6898%">7th</td><td style="width:19.312%">+7</td><td
      style="width:10.8007%">+4</td><td style="width:9.68343%">+2</td><td
      style="width:10.6145%">+2</td><td style="width:41.527%">Bonus feat,
      practiced combatant</td></tr><tr><td style="width:7.6898%">8th</td><td
      style="width:19.312%">+8</td><td style="width:10.8007%">+4</td><td
      style="width:9.68343%">+3</td><td style="width:10.6145%">+3</td><td
      style="width:41.527%">Divine quickness +4, righteous
      leader</td></tr><tr><td style="width:7.6898%">9th</td><td
      style="width:19.312%">+9</td><td style="width:10.8007%">+5</td><td
      style="width:9.68343%">+3</td><td style="width:10.6145%">+3</td><td
      style="width:41.527%">Divine boon 3, symbolic weapon +4</td></tr><tr><td
      style="width:7.6898%">10th</td><td style="width:19.312%">+10</td><td
      style="width:10.8007%">+5</td><td style="width:9.68343%">+3</td><td
      style="width:10.6145%">+3</td><td style="width:41.527%">Unstoppable
      warrior</td></tr></tbody></table><p><br />The following are class features
      of the sentinel prestige class.</p><p><strong>Weapon and Armor
      Proficiency</strong>: Sentinels are proficient with all simple and martial
      weapons, with all types of armor (heavy, medium, and light), and with
      shields (except tower shields).</p><p><strong>Obedience (Ex)</strong>: In
      order to maintain the abilities granted by this prestige class, a sentinel
      must perform a daily obedience to his chosen deity (see page
      10).</p><p><strong>Symbolic Weapon (Su)</strong>: When wielding his
      deity’s favored weapon, the sentinel gains a +1 sacred or profane bonus on
      attack and damage rolls. These bonuses increase by 1 for every 3 levels he
      has in the sentinel prestige class (maximum +4). His deity’s favored
      weapon also functions as a holy (or unholy) symbol when wielded by a
      sentinel.</p><p><strong>Bonus Feat</strong>: At 2nd level and again at 7th
      level, the sentinel gains a bonus feat in addition to those gained from
      normal advancement. These bonus feats must be selected from those listed
      as combat feats. Sentinel levels stack with f ighter levels for the
      purpose of qualifying for feats with a f ighter level prerequisite. If the
      bonus feat selected requires the sentinel to select a specif ic weapon for
      the feat to apply to (such as Greater Weapon Focus), the sentinel must
      select his deity’s favored weapon.</p><p><strong>Divine
      Boon</strong>: As the sentinel gains levels, he gains boons from his
      chosen deity. The nature of these boons varies depending on the sentinel’s
      chosen deity. Each deity grants three boons, each more powerful than the
      last. At 3rd level, the sentinel gains the f irst boon. At 6th level, he
      gains the second boon, and at 9th level, he gains the third boon. Consult
      the Deific Obedience feat on page 210 and the core deity descriptions in
      Chapter 1 for details on these divine boons. When a divine boon grants a
      spell-like ability, the sentinel’s caster level for the spell-like ability
      equals his total character level. This ability allows a sentinel to access
      these boons earlier than with the Deif ic Obedience feat alone; it does
      not grant additional uses of the boons once the character reaches the
      necessary Hit Dice to earn the boons normally.</p><p><strong>Divine
      Quickness (Ex)</strong>: Starting at 4th level, so long as the sentinel
      carries his deity’s favored weapon, he gains a +2 sacred or profane bonus
      on initiative checks. The weapon doesn’t need to be carried in hand, but
      must at least be on his person and accessible enough that he can wield it
      with no greater than a move action. At 8th level, this bonus increases to
      +4.</p><p><strong>Aligned Strike (Su)</strong>: The sentinel’s righteous
      fervor allows him to cut through certain types of damage reduction. At 5th
      level, the sentinel gains the ability to bypass a specific type of damage
      reduction when wielding his deity’s favored weapon. The type of damage
      reduction his weapon bypasses is based on his chosen deity’s alignment.
      The sentinel chooses this type of damage reduction when he gains this
      ability and this choice can’t be changed. He can choose only one of the
      aligned weapon types regardless of how many options he has based on his
      deity’s alignment. For example, a sentinel of Iomedae can treat his weapon
      as axiomatic or holy, but not both. The choices are as
      follows.</p><p><em>Lawful</em>: The sentinel can treat his weapon as
      axiomatic if his deity is lawful.<br /><em>Chaotic</em>: The sentinel can
      treat his weapon as anarchic if his deity is chaotic.<br /><em>Good</em>:
      The sentinel can treat his weapon as holy if his deity is good.<br
      /><em>Evil</em>: The sentinel can treat his weapon as unholy if his deity
      is evil.<br /><em>Neutral</em>: The sentinel can choose one of the four
      other weapon alignment types if his deity is
      neutral.</p><p><strong>Stalwart (Su)</strong>: At 5th level, the sentinel
      gains mental fortitude through his constant meditation and adherence to
      religious tenets. He gains a +2 sacred or profane bonus on saving throws
      to resist divine spells.</p><p><strong>Practiced Combatant (Ex)</strong>:
      At 7th level, the sentinel displays increased combat ability thanks to his
      extensive training and experience. He gains a +2 sacred or profane bonus
      on combat maneuvers he performs with his deity’s favored
      weapon.</p><p><strong>Righteous Leader (Ex)</strong>: At 8th level, the
      sentinel’s commitment to his faith and the deeds he does in the name of
      his god add to his reputation. He gains Leadership as a bonus feat, and is
      considered to qualify for that feat’s special power leadership modifier.
      He takes no penalty for moving around a lot (normally this imposes a –1
      penalty to a character’s leadership score) but he takes double the usual
      penalty for recruiting a cohort of a different alignment (–2 instead of
      –1).</p><p><strong>Unstoppable Warrior (Su)</strong>: At 10th level, the
      sentinel becomes a stalwart paragon demonstrating incredible combat
      prowess. He gains damage reduction based on the alignment of his deity, as
      shown in the table.</p><table class="inner"><tbody><tr><td><strong>Chosen
      Deity’s Alignment</strong></td><td><strong>Damage
      Reduction</strong></td></tr><tr><td>LG</td><td>10/evil</td></tr><tr><td>NG</td><td>5/evil
      and silver</td></tr><tr><td>CG</td><td>5/cold iron and
      evil</td></tr><tr><td>LN</td><td>10/chaotic</td></tr><tr><td>N</td><td>3/-</td></tr><tr><td>CN</td><td>10/lawful</td></tr><tr><td>LE</td><td>10/good</td></tr><tr><td>NE</td><td>5/good
      and silver</td></tr><tr><td>CE</td><td>5/cold iron and
      good</td></tr></tbody></table><p><br />In addition, the sentinel no longer
      falls unconscious when reduced to a negative hit point total, though he
      can take only a single standard or move action while at negative hit
      points, and he continues to lose hit points normally if he is not
      stabilized. Taking standard actions after becoming stable causes the
      sentinel’s wounds to reopen, and he begins losing hit points normally
      again.</p><p>Once per day as a swift action, the sentinel can use <em>cure
      critical wounds</em>, but targeting only himself, with a caster level
      equal to his character level.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - _index: 0
        level: 5
        name: Aligned Strike (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.uIaU12ncJ26gg6oE
      - _index: 1
        level: 2
        name: Bonus Feat
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.vp1VzQpH7ADN0qLp
      - _index: 2
        level: 3
        name: Divine Boon
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.FKiKyKrFelFvUTeL
      - _index: 3
        level: 4
        name: Divine Quickness (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.zFGzzj29uhZLSCB4
      - _index: 4
        level: 1
        name: Obedience (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.sCMGiuQQ3iCpx5Bf
      - _index: 5
        level: 7
        name: Practiced Combatant (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.tZfAwoFxjeQynFEY
      - _index: 6
        level: 8
        name: Righteous Leader (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.L8eMZWSaDNW4fv64
      - _index: 7
        level: 5
        name: Stalwart (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.gHlXKXXFXQkgAa2B
      - _index: 8
        level: 1
        name: Symbolic Weapon (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.kP2ZUR0IYYuE3VYN
      - _index: 9
        level: 10
        name: Unstoppable Warrior (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.rZpTaq4uRnzfuw3j
  savingThrows:
    fort:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO9267
      pages: '202'
  subType: prestige
  tag: sentinel
  weaponProf:
    value:
      - sim
      - mar
type: class

