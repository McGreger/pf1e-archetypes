_id: bxAyOIGsf0RIBioK
_key: '!items!bxAyOIGsf0RIBioK'
img: systems/pf1/icons/skills/yellow_39.jpg
name: Horizon Walker
system:
  bab: high
  classSkills:
    clm: true
    dip: true
    han: true
    kge: true
    kna: true
    kpl: true
    lin: true
    per: true
    ste: true
    sur: true
    swm: true
  description:
    value: >-
      <p>Those whose wanderlust drives them to push the boundary of safe
      environments sometimes seek the calling of the horizon walker. Horizon
      walkers are masters of travel, always seeking to find the safe ways
      through inhospitable terrain. They are comfortable in places others speak
      of only in hushed whispers, and they can both venture forth onto untrod
      paths themselves and guide the less-traveled through such hazardous
      lands.</p><p>Though rangers are most likely to be attracted to the
      ever-wandering life common to horizon walkers, barbarians, fighters, and
      rogues have also been known to find it appealing. Horizon walkers are most
      common in areas on the edge of civilization, where they can easily spend
      time away from the known and boring streets of settled
      lands.</p><p><strong>Role</strong>: Horizon walkers have the accuracy and
      resilience to face frontline combat. They also have the skills and
      abilities to make excellent scouts, often taking the role of the first
      member of a group into danger and the last one out. Of course, walkers
      shine when adventuring in areas that match their many favored terrains,
      but many of the tricks they pick up in specific regions have application
      in a wide range of situations, allowing a horizon walker to be more mobile
      and resourceful than typical combatants.</p><p><strong>Alignment</strong>:
      Anyone can crave new vistas and constant travel, and thus a horizon walker
      can be of any alignment.</p><h2>Requirements</h2><p>To qualify to become a
      horizon walker, a character must fulfill all the following
      criteria.</p><p><strong>Skills</strong>: Knowledge (geography) 6 ranks.<br
      /><strong>Feats</strong>: Endurance.</p><h2>Class Skills</h2><p>The
      Horizon Walker's class skills are Climb (Str), Diplomacy (Cha), Handle
      Animal (Cha), Knowledge (geography) (Int), Knowledge (nature) (Int),
      Knowledge (the planes) (Int), Linguistics (Int), Perception (Wis), Stealth
      (Dex), Survival (Wis), and Swim (Str).</p><p><strong>Skill Points at each
      Level</strong>: 6 + Int modifier.<br /><strong>Hit Die</strong>:
      d10.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:7.88211%"><strong>Level</strong></td><td
      style="width:19.8646%"><strong>Base Attack Bonus</strong></td><td
      style="width:10.987%"><strong>Fort Save</strong></td><td
      style="width:9.86965%"><strong>Ref Save</strong></td><td
      style="width:10.8007%"><strong>Will Save</strong></td><td
      style="width:40.2235%"><strong>Special</strong></td></tr><tr><td
      style="width:7.88211%">1st</td><td style="width:19.8646%">+1</td><td
      style="width:10.987%">+1</td><td style="width:9.86965%">+0</td><td
      style="width:10.8007%">+0</td><td style="width:40.2235%">Favored
      terrain</td></tr><tr><td style="width:7.88211%">2nd</td><td
      style="width:19.8646%">+2</td><td style="width:10.987%">+1</td><td
      style="width:9.86965%">+1</td><td style="width:10.8007%">+1</td><td
      style="width:40.2235%">Favored terrain, terrain mastery</td></tr><tr><td
      style="width:7.88211%">3rd</td><td style="width:19.8646%">+3</td><td
      style="width:10.987%">+2</td><td style="width:9.86965%">+1</td><td
      style="width:10.8007%">+1</td><td style="width:40.2235%">Terrain
      dominance</td></tr><tr><td style="width:7.88211%">4th</td><td
      style="width:19.8646%">+4</td><td style="width:10.987%">+2</td><td
      style="width:9.86965%">+1</td><td style="width:10.8007%">+1</td><td
      style="width:40.2235%">Favored terrain, terrain mastery</td></tr><tr><td
      style="width:7.88211%">5th</td><td style="width:19.8646%">+5</td><td
      style="width:10.987%">+3</td><td style="width:9.86965%">+2</td><td
      style="width:10.8007%">+2</td><td style="width:40.2235%">Favored
      terrain</td></tr><tr><td style="width:7.88211%">6th</td><td
      style="width:19.8646%">+6</td><td style="width:10.987%">+3</td><td
      style="width:9.86965%">+2</td><td style="width:10.8007%">+2</td><td
      style="width:40.2235%">Terrain dominance, terrain mastery</td></tr><tr><td
      style="width:7.88211%">7th</td><td style="width:19.8646%">+7</td><td
      style="width:10.987%">+4</td><td style="width:9.86965%">+2</td><td
      style="width:10.8007%">+2</td><td style="width:40.2235%">Favored
      terrain</td></tr><tr><td style="width:7.88211%">8th</td><td
      style="width:19.8646%">+8</td><td style="width:10.987%">+4</td><td
      style="width:9.86965%">+3</td><td style="width:10.8007%">+3</td><td
      style="width:40.2235%">Favored terrain, terrain mastery</td></tr><tr><td
      style="width:7.88211%">9th</td><td style="width:19.8646%">+9</td><td
      style="width:10.987%">+5</td><td style="width:9.86965%">+3</td><td
      style="width:10.8007%">+3</td><td style="width:40.2235%">Terrain
      dominance</td></tr><tr><td style="width:7.88211%">10th</td><td
      style="width:19.8646%">+10</td><td style="width:10.987%">+5</td><td
      style="width:9.86965%">+3</td><td style="width:10.8007%">+3</td><td
      style="width:40.2235%">Favored terrain, master of all
      lands</td></tr></tbody></table><p><br />The following are class features
      of the horizon walker prestige class. </p><p><strong>Weapon and Armor
      Proficiency</strong>: A horizon walker gains no proficiency with any
      weapon or armor. </p><p><strong>Favored Terrain</strong>: At 1st level, a
      horizon walker may select a favored terrain from the ranger Favored
      Terrains table (<em>Pathfinder RPG Core Rulebook </em>65). This works
      exactly like the ranger favored terrain ability. The horizon walker gains
      an additional favored terrain at 2nd, 4th, 5th, 7th, 8th and 10th level,
      and he can increase the bonus from an existing favored terrain as
      described in the ranger ability. If the horizon walker has abilities from
      other classes that only work in a favored terrain (such as a ranger's
      camouflage and hide in plain sight abilities), those abilities work in
      favored terrains selected as a horizon walker. </p><p><strong>Terrain
      Mastery</strong>: At 2nd level, a horizon walker selects a favored terrain
      to master. When within this terrain the horizon walker may, as a move
      action, grant a +2 bonus on Climb, Stealth, Perception and Survival checks
      made by all allies within 30 feet who can see and hear him. The character
      masters one additional terrain at 4th, 6th, and 8th levels. </p><p>Mastery
      of each terrain has additional benefits, outlined below; these benefits
      apply to the horizon walker at all times whether or not he is in the
      relevant terrain. </p><p><em>Astral Plane</em>: The horizon walker's fly
      speed increases by +30 feet on planes with no gravity or subjective
      gravity. <br /><em>Cold</em>: The horizon walker gains cold resistance 10.
      <br /><em>Desert</em>: The horizon walker gains immunity to exhaustion;
      anything that would cause him to become exhausted makes him fatigued
      instead. <br /><em>Ethereal Plane</em>: The horizon walker ignores the 20%
      concealment miss chance from fog and mist, and treats total concealment
      from these sources as concealment. <br /><em>Forest</em>: The horizon
      walker gains a +4 competence bonus on Stealth checks. <br
      /><em>Jungle</em>: The horizon walker gains a +4 competence bonus on
      Escape Artist checks and increases his CMD against grapple maneuvers by
      +4. <br /><em>Mountain</em>: The horizon walker gains a +4 competence
      bonus on Climb checks and does not lose his Dexterity modifier to AC while
      climbing. <br /><em>Plains</em>: The horizon walker's movement is not
      reduced by wearing medium armor or carrying a medium load. <br /><em>Plane
      of Air</em>: The horizon walker gains a +4 competence bonus on Fly checks
      and +1 competence bonus on all attack and damage rolls against flying
      creatures. He gains the ability to breathe air if he cannot already do so.
      <br /><em>Plane of Earth</em>: The horizon walker gains DR 1/adamantine.
      <br /><em>Plane of Fire</em>: The horizon walker gains fire resistance 10.
      <br /><em>Plane of Water</em>: The horizon walker gains a +4 competence
      bonus on Swim checks and a +1 competence bonus on all attack and damage
      rolls against swimming creatures. He gains the ability to breathe water if
      he cannot already do so. <br /><em>Plane, aligned</em>: If the horizon
      walker selects a plane with an alignment trait, he can choose to detect as
      that alignment (fooling all forms of magic divination) as an immediate
      action; this benefit lasts until he dismisses it (a free action). <br
      /><em>Swamp</em>: The horizon walker gains a +4 competence bonus on
      Perception checks. <br /><em>Underground</em>: The horizon walker gains
      Blind-Fight as a bonus feat. <br /><em>Urban</em>: The horizon walker
      gains a +4 competence bonus on Diplomacy checks. <br /><em>Water</em>: The
      horizon walker gains a +4 competence bonus on Swim checks and +1
      competence bonus on all attack and damage rolls against swimming
      creatures. </p><p><strong>Terrain Dominance</strong>: At 3rd level, a
      horizon walker learns total dominance over one terrain he has already
      selected for terrain mastery. When dealing with creatures native to that
      terrain, the horizon walker treats his favored terrain bonus for that
      terrain as a favored enemy bonus (as the ranger class feature) against
      those creatures.This bonus overlaps (does not stack with) bonuses gained
      when fighting a favored enemy. </p><p>Each terrain dominance grants
      additional abilities, detailed below. When the horizon walker gains a new
      terrain dominance he may, if he prefers, instead pick an additional
      terrain mastery. </p><p><em>Astral Plane</em>: The horizon walker gains a
      +1 competence bonus on attack and damage rolls against outsiders. He gains
      <em>dimension door </em>as a spell-like ability a number of times per day
      equal to 3 + the character's Wisdom modifier (caster level equal to the
      character's level). <br /><em>Cold</em>: The horizon walker gains cold
      resistance 20 (this replaces the character's cold resistance from mastery
      of the cold terrain) and a +1 competence bonus on all attack and damage
      rolls against creatures of the cold subtype. <br /><em>Desert</em>: The
      horizon walker gains fire resistance 10 and immunity to fatigue. <br
      /><em>Ethereal Plane</em>: The walker gains <em>ethereal jaunt </em>as a
      spell-like ability once per day (caster level equal to the character's
      level). He must be at least 7th level before selecting this power. <br
      /><em>Forest</em>: The horizon walker gains <em>hallucinatory terrain
      </em>as a spell-like ability a number of times per day equal to 3 + the
      character's Wisdom modifier (caster level equal to the character's level).
      The horizon walker can only use this ability to create illusory forests.
      <br /><em>Jungle</em>: The horizon walker gains <em>charm monster </em>as
      a spell-like ability a number of times per day equal to 3 + the
      character's Wisdom modifier (caster level equal to the character's level).
      This charm only affects animals, magical beasts, and creatures primarily
      found in the jungle. <br /><em>Mountain</em>: The horizon walker gains DR
      2/adamantine. <br /><em>Plains</em>: The walker's base speed increases by
      +10 feet. <br /><em>Plane of Air</em>: The horizon walker gains <em>fly
      </em>as a spell-like ability a number of times per day equal to 3 + the
      character's Wisdom modifier (caster level equal to the character's level).
      <br /><em>Plane of Earth</em>: The horizon walker gains tremorsense with a
      range of 30 feet. He must be at least 5th level before selecting this
      plane for this ability. <br /><em>Plane of Fire</em>: The horizon walker
      gains fire resistance 20 (this replaces the character's file resistance
      from mastery of the Plane of Fire terrain) and a +1 competence bonus on
      all attack and damage rolls again creatures of the fire subtype. <br
      /><em>Plane of Water</em>: The horizon walker's movements and actions are
      not hampered when underwater. This allows him to speak, make attacks, and
      cast spells normally underwater (as if using <em>freedom of
      movement</em>). <br /><em>Plane, aligned</em>: For the purpose of
      bypassing damage reduction, the horizon walker's manufactured and natural
      weapons count as the opposite alignment of his chosen plane of dominance.
      If his chosen plane has more than one alignment type, he must choose one
      of those types for this ability. For example, if he selects “Hell” (an
      evil, lawful plane) for his terrain dominance ability, he may choose
      “evil” or “lawful,” which means his attacks bypass good or chaotic damage
      reduction, respectively. Alternatively, if native creatures of his chosen
      plane are vulnerable to a particular special material (such as cold iron
      or silver), he may choose to have his weapons bypass damage reduction as
      if they were that special material. If he selects this plane more than
      once for his terrain dominance ability, his attacks count as an additional
      alignment or special material (such as “good and silver” or “chaotic and
      good”). <br /><em>Swamp</em>: The horizon walker gains tremorsense with a
      30-foot range. He must be 5th level before selecting this power. <br
      /><em>Underground</em>: The horizon walker gains darkvision with a range
      of 60 feet. If he already has darkvision 60 feet, its range extends by 60
      feet. <br /><em>Urban</em>: The horizon walker gains <em>charm person
      </em>as a spell-like ability a number of times per day equal to 3 + the
      character's Wisdom modifier (caster level equal to the character's level).
      <br /><em>Water</em>: The horizon walker gains a swim speed of 20 feet. If
      he already has a natural swim speed, his swim speed increases by +20 feet.
      </p><p><strong>Master of All Lands (Su)</strong>: At 10th level, the
      horizon walker becomes familiar with and comfortable in all possible
      terrains. His terrain bonus in all favored terrains increases by +2, and
      he treats all other terrains as if they were favored terrains (+2 bonus).
      If a naturally occurring condition of temperature or weather requires a
      check or saving throw, he automatically succeeds. All allies within 60
      feet of him gain a +2 bonus on these checks and saves; if the horizon
      walker is in a mastered terrain, this bonus increases to +4.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - _index: 0
        level: 2
        name: Terrain Mastery
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.RIjKEsxj9nYs2W0f
      - _index: 1
        level: 3
        name: Terrain Dominance
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.WXjbAV4Cr2mmbHBq
      - _index: 2
        level: 10
        name: Master of All Lands (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.uKVudFr94iVPQK0X
      - _index: 3
        level: 1
        name: Favored Terrain
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.3lFNqsle8Wiv9bCL
  savingThrows:
    fort:
      value: high
  skillsPerLevel: 6
  sources:
    - id: PZO1115
      pages: '265'
  subType: prestige
  tag: horizonWalker
type: class

