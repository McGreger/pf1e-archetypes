_id: ruIJraTQNUdgCZxx
_key: '!items!ruIJraTQNUdgCZxx'
folder: Xp99iiO8M955HvSy
img: systems/pf1/icons/items/inventory/lute.jpg
name: Bard (Brazen Deceiver)
system:
  associations:
    classes:
      - Bard
  description:
    value: >-
      <p>Where other thieves use stealth or intimidation to achieve their goals,
      the brazen deceiver depends on lies. The Council of Thieves often sends
      brazen deceivers to acquire blackmail materials, arrange the release of
      incarcerated agents, or otherwise manipulate events in the Council’s
      favor. Brazen deceivers’ techniques come from a dark source. During the
      reformation of the Council, Sabriune and Aspexia acquired the shattered
      fragments of an ancient relic called the Totemrix, which belonged to the
      long-deceased demon lord of shadows, Vyriavaxus. This particularly
      intrigued Sabriune, for it was her demonic patron Nocticula who slew
      Vyriavaxus. Drawing upon Sabriune’s heretical faith, Aspexia’s
      psychometric mastery, and Westcrown’s lingering psychic trauma from an
      oppressive shadow curse that plagued its nights for many years, the
      Council leaders were able to coax remnants of power from the broken
      artifact. All brazen deceivers are imbued with a fragment of that power
      during their initiation—not enough to damn a soul or shift alignment, but
      just enough to give these bards access to the sinister energies they need
      to deceive.</p><p><strong>Deceptive Tale (Su)</strong>: A brazen deceiver
      learns the deceptive tale bardic performance, allowing him to weave magic
      into his lies and imbue the most fantastic claims with the appearance of
      truth. While the brazen deceiver maintains this performance, he takes half
      the normal penalty on Bluff checks for unlikely lies (rounding down to
      –2). At 5th level, this effect also applies to Bluff checks for
      far-fetched lies, and at 11th level, it applies to Bluff checks for
      impossible lies. Deceptive tale relies on audible components.</p><p>This
      replaces the countersong and distraction bardic
      performances.</p><p><strong>Shameless Scoundrel (Ex)</strong>: A brazen
      deceiver adds half his level (minimum +1) on Bluff, Disguise, and Stealth
      checks.</p><p>This ability replaces bardic
      knowledge.</p><p><strong>Blatant Subtlety (Ex)</strong>: At 2nd level, a
      brazen deceiver has mastered the art of using magic without being
      detected. The brazen deceiver gains <a
      href="https://aonprd.com/FeatDisplay.aspx?ItemName=Spellsong">Spellsong</a>
      as a bonus feat. Observers do not automatically recognize his bardic
      performances as anything other than ordinary speech or performance. Those
      specifically looking for abnormal effects must succeed at a Sense Motive
      check (DC = 10 + half the brazen deceiver’s bard level + the brazen
      deceiver’s Charisma modifier) to detect his performances.</p><p>This
      ability replaces well-versed.</p><p><strong>Invoke Vyriavaxus
      (Ex)</strong>: Westcrown suffered under the shadow curse for many years,
      and a brazen deceiver has a tiny piece of that darkness lodged in his
      soul; as his skills increase, he learns to command this shadowy power. A
      brazen deceiver adds the following spells to his bard spells known at the
      listed class levels. At 2nd level, the brazen deceiver adds
      @UUID[Compendium.pf1.spells.Item.n390v9tbwxya1tz4]{bleed} and
      @UUID[Compendium.pf1.spells.Item.t1w2zq5fx7mislq8]{touch of fatigue} to
      his 0-level bard spells known. At 6th level, the brazen deceiver adds
      @UUID[Compendium.pf1.spells.Item.tsndfcfijmgxs37p]{darkness} and
      @UUID[Compendium.pf1.spells.Item.mz9yr5yhr0f3oqtp]{darkvision} to his
      2nd-level bard spells known. At 10th level, he adds
      @UUID[Compendium.pf1.spells.Item.ed7fvzc2p4m6n7a2]{shadow conjuration} and
      @UUID[Compendium.pf1.spells.Item.cmwcavfyc1vbehy8]{shadow step} to his
      4th-level bard spells known. At 14th level, he adds
      @UUID[Compendium.pf1.spells.Item.2q48ogrz3840xwi7]{shadow evocation} and
      @UUID[Compendium.pf1.spells.Item.btoow6tyv39443gh]{shadow walk} to his
      5th-level bard spells known. At 18th level, he adds
      @UUID[Compendium.pf1.spells.Item.44nj6jzo81o9mng9]{shadow conjuration,
      greater} and <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=shadow%20evocation,%20greater">greater
      shadow evocation</a> to his 6th-level bard spells known.</p><p>This
      ability replaces versatile performance.</p><p>Devil’s Tongue (Ex): At 5th
      level, a brazen deceiver beguiles others with astonishing skill. This
      functions as the lore master ability, but its effects apply to Bluff
      checks instead of Knowledge skill checks.</p><p>This ability replaces lore
      master.</p>
  sources:
    - id: PZO1138
      pages: '50'
  subType: misc
  tags:
    - Brazen Deceiver
type: feat

