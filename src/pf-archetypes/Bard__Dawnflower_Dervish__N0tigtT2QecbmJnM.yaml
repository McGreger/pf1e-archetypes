_id: N0tigtT2QecbmJnM
_key: '!items!N0tigtT2QecbmJnM'
folder: Xp99iiO8M955HvSy
img: systems/pf1/icons/items/inventory/lute.jpg
name: Bard (Dawnflower Dervish)
system:
  associations:
    classes:
      - Bard
  description:
    value: >-
      <p>Although Sarenrae is seen mainly as a goddess of healing and redemption
      in most parts of the Inner Sea, her stern, evil-smiting element is more
      common in areas like Qadira, Osirion, and Katapesh. Many of the
      Dawnflower’s disciples from these lands become Dawnflower dervishes,
      religious mystics who use a spinning dance as part of their worship. Many
      bards of her faith hone their skills with dance and scimitar to become
      dervish dancers (see Ultimate Combat), but some tread a similar path
      focused more on magic and healing than swordplay. These are the Dawnflower
      dervishes. A Dawnflower dervish has the following class
      features.</p><p><strong>Weapon and Armor Proficiency</strong>: Dawnflower
      dervishes gain weapon proficiency with the scimitar. This ability replaces
      their proficiency with the rapier and whip.</p><p><strong>Deity</strong>:
      A Dawnflower dervish must be a worshiper of <a
      href="https://aonprd.com/DeityDisplay.aspx?ItemName=Sarenrae">Sarenrae</a>.
      A dervish who abandons or betrays this faith reverts to a standard
      bard.</p><p><strong>Battle Dance</strong>: A Dawnflower dervish is trained
      in the use of the Perform skill, especially dance, to create magical
      effects on himself. This works like bardic performance, except that the
      Dawnflower dervish’s performances grant double their normal bonuses, but
      these bonuses only affect him. He does not need to be able to see or hear
      his own performance. Battle dancing is treated as bardic performance for
      the purposes of feats, abilities, and effects that affect bardic
      performance, except that battle dancing does not benefit from the
      Lingering Performance feat or any other ability that allows a bardic
      performance to grant bonuses after it has ended. The benefits of battle
      dancing apply only when the bard is wearing light or no armor. Like bardic
      performance, battle dancing cannot be maintained at the same time as other
      performance abilities.</p><p>Starting a battle dance is a move action, but
      it can be maintained each round as a free action. Changing a battle dance
      from one effect to another requires the Dawnflower dervish to stop the
      previous performance and start the new one as a move action. Like a bard,
      a Dawnflower dervish’s performance ends immediately if he is killed,
      paralyzed, stunned, knocked unconscious, or otherwise prevented from
      taking a free action each round. A Dawnflower dervish cannot perform more
      than one battle dance at a time. At 10th level, a Dawnflower dervish can
      start a battle dance as a swift action instead of a move
      action.</p><p>When the Dawnflower dervish uses the inspire courage,
      inspire greatness, or inspire heroics bardic performance types as battle
      dances, these performance types only provide benefit to the Dawnflower
      dervish himself. All other types of bardic performance work normally
      (affecting the bard and his allies, or the bard’s enemies, as
      appropriate). This ability alters the standard bardic performance
      ability.</p><p><strong>Dervish Dance (Ex)</strong>: A Dawnflower dervish
      gains the <a
      href="https://aonprd.com/FeatDisplay.aspx?ItemName=Dervish%20Dance">Dervish
      Dance</a> feat as a bonus feat. This ability replaces bardic
      knowledge.</p><p><strong>Spinning Spellcaster (Ex)</strong>: At 5th level,
      a Dawnflower dervish gains a +4 bonus on concentration checks to cast
      spells defensively. This ability replaces lore
      master.</p><p><strong>Meditative Whirl (Ex)</strong>: At 8th level, when
      using battle dance, the Dawnflower dervish can enter a trancelike state
      where his spinning motion represents the movement of the planets around
      the sun, and his spirit is attuned to the healing aspects of Sarenrae. By
      spending a move action focusing on his whirling, the dervish can apply the
      @UUID[Compendium.pf1.feats.Item.wFoLrh5FeeqBXsQ8]{Quicken Spell} feat to
      any cure spell he is about to cast (effectively spending a move action and
      swift action to cast the spell). This does not alter the level of the
      spell or the casting time. The dervish can use this ability once per day
      at 8th level and one additional time per day for every two dervish levels
      he has beyond 8th. This ability replaces the dirge of doom bardic
      performance.</p>
  sources:
    - id: PZO9237
      pages: '34'
  subType: misc
  tags:
    - Dawnflower Dervish
type: feat

