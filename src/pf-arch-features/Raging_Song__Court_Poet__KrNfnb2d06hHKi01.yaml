_id: KrNfnb2d06hHKi01
_key: '!items!KrNfnb2d06hHKi01'
img: systems/pf1/icons/items/inventory/horn-drinking.jpg
name: Raging Song (Court Poet)
system:
  abilityType: none
  associations:
    classes:
      - Skald
  description:
    value: >-
      <p><strong>Level</strong>: 1</p><p>A skald is trained to use music,
      oration, and similar performances to inspire his allies to feats of
      strength and ferocity. At 1st level, a skald can use this ability for a
      number of rounds per day equal to 3 + his Charisma modifier. For each
      level thereafter, he can use raging song for 2 additional rounds per
      day.Starting a raging song is a standard action, but it can be maintained
      each round as a free action. A raging song cannot be disrupted, but it
      ends immediately if the skald is killed, paralyzed, stunned, knocked
      unconscious, or otherwise prevented from taking a free action each round
      to maintain it. A raging song counts as the bard's bardic performance
      special ability for any effect that affects bardic performances. A skald
      may learn bard masterpieces (Pathfinder RPG Ultimate Magic 21).</p><p>A
      raging song has audible components, but not visual components. Affected
      allies must be able to hear the skald for the song to have any effect. A
      deaf skald has a 20% chance to fail when attempting to use a raging song.
      If he fails this check, the attempt still counts against his daily limit.
      Deaf creatures are immune to raging songs.</p><p>If a raging song affects
      allies, when the skald begins a raging song and at the start of each
      ally's turn in which they can hear the raging song, the skald's allies
      must decide whether to accept or refuse its effects. This is not an
      action. Unconscious allies automatically accept the song. If accepted, the
      raging song's effects last for that ally's turn or until the song ends,
      whichever comes first.</p><p>At 7th level, a skald can start a raging song
      as a move action instead of a standard action. At 13th level, a skald can
      start a raging song as a swift action instead.</p><p><i>Insightful
      Contemplation (Su): </i>At 1st level, affected allies gain a +2 morale
      bonus to Intelligence and Charisma and a +1 morale bonus on Will saving
      throws, but they also take a -1 penalty to AC. While under the effects of
      insightful contemplation, allies other than the court poet can't use any
      Strength-based skills or make any physical effort that requires a
      Constitution check. At 4th level and every 4 skald levels thereafter, the
      song's bonus on Will saves increases by 1; the penalty to AC doesn't
      change. At 8th and 16th levels, the song's bonuses to Intelligence and
      Charisma increase by 2. (Unlike the barbarian's rage ability, those
      affected are not fatigued after the song ends.)</p><p><i>Song of
      Inspiration (Su): </i>At 6th level, a court poet can use raging song to
      inspire her allies to greater mental clarity. Once each round while the
      court poet uses this performance, allies within 60 feet who can hear her
      can add 1/2 the court poet's skald level to a single Wisdom check or
      Wisdom-based skill check.</p><p><i>Dirge of Doom (Su): </i>At 10th level,
      a skald can create a sense of growing dread in his enemies, causing them
      to become shaken. This only affects enemies that are within 30 feet and
      able to hear the skald's performance. The effect persists for as long as
      the enemy is within 30 feet and the skald continues his performance. This
      cannot cause a creature to become frightened or panicked, even if the
      targets are already shaken from another effect. This is a sonic
      mind-affecting fear effect, and relies on audible
      components.</p><p><i>Song of the Fallen (Su): </i>At 14th level, a skald
      can temporarily revive dead allies to continue fighting, with the same
      limitations as raise dead. The skald selects a dead ally within 60 feet
      and expends 1 round of raging song to bring that ally back to life. The
      revived ally is alive but staggered. Each round, the skald may expend
      another 1 round of raging song to keep that ally alive for another round.
      The ally automatically dies if the skald ends this performance or is
      interrupted. The skald may revive multiple allies with this ability
      (either at the same time or over successive rounds) but must expend 1
      round of raging song per revived ally per round to maintain the
      effect.</p>
  subType: classFeat
  tags:
    - Court Poet
type: feat

